$(document).on('click', '.table a[data-delete], .table button[data-delete]', function(e) {
  e.preventDefault();
  var el = this;
  if(confirm('Are you sure?')) {
    $(el).parents('tr').first().css('opacity',0.5);
    $.post( $(this).attr('href'), { "_method" : "delete" }, function(res) {
      $(el).parents('tr').first().fadeOut();
    });
  }
});

var fixHelper = function(e, ui) {
  ui.children().each(function() {
    $(this).width($(this).width());
  });
  return ui;
};


function showMessage(msg, error) {
  var c = error == undefined || error ? "success" : "danger";
  $('<div class="alert alert-'+c+'" />')
    .append('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>')
    .append(msg)
    .appendTo('#messages');
}

$(document).on('click','a[data-confirm]',function(e) {
    if(!confirm($(this).data('confirm'))) {
      e.preventDefault();
    }
  });

$('a[data-method="delete"]').click(function(e) {    
  
    e.preventDefault();
    var message = $(this).data('confirm')? $(this).data('confirm') : 'Are you sure?';

    if(confirm(message)) {  
    var form = $('<form />').attr('method','post').attr('action',$(this).attr('href'));
  
    $('<input />').attr('type','hidden').attr('name','_method').attr('value','delete').appendTo(form);
    
    $('body').append(form);
    form.submit();
  }

  
  });