var customInterpolationApp = angular.module('app', ['ui.bootstrap']);

customInterpolationApp.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

function waypointCntrl($scope,$http,$templateCache,$compile){
    $scope.counter=0;
    $scope.waypointelement = [];
    $scope.checkelement=[];
    $scope.formelement=[];
    $scope.formData = {};

    

    $scope.newItem = function($event){
        $scope.counter++;
        $scope.waypointelement.push( {id:$scope.counter, waypoint: '', duration: ''} );
        $event.preventDefault();
    }
    $scope.removeItem = function($event){
        $scope.counter--;
    }
    $scope.fetch = function() {
        $scope.method = 'POST';
        $scope.url = '/filter';

        $scope.formData.total_time=document.getElementById('total_time').value;
        $scope.formData.total_distance=document.getElementById('total_distance').value;
        console.log($scope.formData);
        $http({method: $scope.method, url:$scope.url, cache:$templateCache,data:$scope.formData}).
        success(function(data, status) {
          $scope.status = status;
            var Parent = document.getElementById('grid');
            while(Parent.hasChildNodes())
            {
                Parent.removeChild(Parent.firstChild);
            }
          console.log(data);
          for(var i=0;i<data.length;i++)
          {
            var rootdiv=document.createElement('tr');
            rootdiv.setAttribute('class','tb_page');
            rootdiv.innerHTML="<td><img src=http://platinum-luxury.local/uploads/car-thumb/"+data[i].photo+" height=100 width=150/></td><td><label>Car Name-</label><br/><label>"+data[i].name+"</label></td><td>"+data[i].total_time+" mins</td><td>"+data[i].base_rate+"</td><td><a href='javascript:void(0);' class='btn btn-primary book_btn' data-carid="+data[i].id+" ng-click=createForm($event,"+data[i].id+","+data[i].base_rate+")>Book Now</a></td>";
            $compile(rootdiv)($scope);
            document.getElementById('grid').appendChild(rootdiv);
          }
        }).
        error(function(data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });
    }   

    $scope.send = function() {
        $scope.method = 'POST';
        $scope.url = '/send';

        console.log($scope.userData);
        $http({method: $scope.method, url:$scope.url,cache:$templateCache,data:$scope.userData}).
        success(function(data, status) {
          $scope.status = status;
          console.log(data); 
        }).
        error(function(data, status) {
          $scope.data = data || "Request failed";
          $scope.status = status;
      });

        $scope.gender=$scope.userData['gender'];
        $scope.name=$scope.userData['name'];
        $scope.surname=$scope.userData['surname'];
        $scope.email=$scope.userData['email'];
        $scope.phone=$scope.userData['phone'];
        $scope.from=$scope.userData['from'];
        $scope.to=$scope.userData['to'];
        $scope.message=$scope.userData['message'];
        $scope.payment=$scope.userData['payment'];
    }

    $scope.createForm= function($event,$id,$price){
        $scope.counter++;
        $scope.price=$price;

    }
    
}




