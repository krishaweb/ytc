<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

// Define a destination
$targetPath = '../tmp'; // Relative to the root

if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];	 
  	$fileParts = pathinfo($_FILES['Filedata']['name']);
  	$filename = uniqid().'.'.$fileParts['extension'];
	$targetFile = rtrim($targetPath,'/') . '/' . $filename;
  
	if(move_uploaded_file($tempFile,$targetFile)){
		echo $filename;
	} else {
    header("HTTP/1.1 500 Not uploaded");		
	}
}
