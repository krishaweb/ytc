<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6LdmBekSAAAAACGE_AH0DcctSBi2H4SuYCzTykiQ',
	'private_key'	=> '6LdmBekSAAAAAG5VfmEhYVoMvuuA3DJZxM6kpQPb',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',
	
);