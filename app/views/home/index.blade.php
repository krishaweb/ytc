@section('title')
{{ 'Home' }}
@stop

@section('content')
<div class="banner">
<ul>
<li><img src="{{asset('img/index-banner.jpg')}}">
<div class="wrap">
<div class="banner-text">
<h1>Simplify Your Training Choices</h1>
<h3>Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montesnascetur</h3>
<div>
  <div class="play-now-btn"><a href="#"><img src="{{asset('img/play-icon.png')}}">Play Video</a></div>
    <div class="search-now-btn"><a href="{{URL::to('choose-training')}}">Search Now!!</a></div>
</div>
</div>
</div>
</li>
</ul>
</div>

<section class="about">
  <div class="wrap">
      <div class="about-left"><h2>About Our Company</h2>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </p>
<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
<div class="read-more"><a href="{{URL::to('about-us')}}">Read More</a></div>
        </div>
      <div class="about-right"><img src="{{asset('img/about-img.jpg')}}"></div>
      <div class="c"></div>
    </div>
</section>

<div class="staps">
  <div class="wrap">
      <ul>
          <li>
              <h5>Step 1</h5>
        <h4>Search</h4>
                <div class="step-disc">Find the right training for YOU.</div>
            </li>
            
          <li>
              <h5>Step 2</h5>
                <h4>Results</h4>
                <div class="step-disc">Our Results page allows you direct access to the right training. ‘No Middle Man’</div>
            </li>
            
          <li>
              <h5>Step 3</h5>
                <h4>Your Choice</h4>
                <div class="step-disc">Choose your right training and contact directly.</div>
            </li>
        </ul>
        <div class="c"></div>
        <div class="find-right-training-btn"><a href="{{URL::to('choose-training')}}">Find The Right Training</a></div>
    
    </div>
</div>

<div class="trusted-source">
  <div class="trusted-source-left"><img src="{{asset('img/trusted-store.jpg')}}"></div>
    <div class="trusted-source-right">
      <div class="trusted-source-right-text">
          <h2>A Trusted Source For Knowledge.</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </p>
            
            <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies.</p>
        </div>
    </div>
    <div class="c"></div>
</div>

<div class="footer-section">
  <div class="wrap">
      <ul>
          <li>
              <h4>Resources</h4>
                <div class="step-disc">Simplfy the process with our tools and templates.</div>
                <div class="know-more"><a href="{{URL::to('resources')}}">Know More</a></div>
            </li>
            
          <li>
              <h4>Latest News</h4>
                <div class="step-disc">Keep up to date with the latest News and Views.</div>
                <div class="know-more"><a href="{{URL::to('news')}}">Know More</a></div>
            </li>
            
          <li>
              <h4>For Providers</h4>
                <div class="step-disc">Get Involved and Access your customer directly through us.</div>
                <div class="know-more"><a href="{{URL::to('for-providers')}}">Know More</a></div>
            </li>
        </ul>
        <div class="c"></div>
    </div>
</div>
@stop