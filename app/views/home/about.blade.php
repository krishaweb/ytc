@section('title')
{{ 'About Us' }}
@stop

@section('content')
<div class="banner-inner">
    <h1>What We Do</h1>
</div>

<div class="aboutus">
    <div class="wrap">
        <h2>An Introduction With Us</h2>
        <div class="aboutus-left">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ultricies nec, </p>
      </div>
        <div class="aboutus-right">
            <ul>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                <li>Cum sociis natoque penatibus et magnis dis parturient </li>
                <li>Nulla consequat massa quis enim. </li>
                <li>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. </li>
                <li>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. </li>
                <li>Cum sociis natoque penatibus et magnis dis parturient </li>
                <li>Nulla consequat massa quis enim. </li>
            </ul>
        </div>
        <div class="c"></div>
            <h2>Why Working With Us Is Advantageous!</h2>
        <div class="aboutus-left">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. 
<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur </p>

            <ul>
                <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                <li>Cum sociis natoque penatibus et magnis dis parturient </li>
                <li>Nulla consequat massa quis enim. </li>
                <li>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. </li>
                <li>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. </li>
                <li>Cum sociis natoque penatibus et magnis dis parturient </li>
                <li>Nulla consequat massa quis enim. </li>
            </ul>
            </div>
        <div class="aboutus-right"><img src="{{asset('img/about-us-img.jpg')}}"></div>
        <div class="c"></div>
        
        
  </div>
</div>

<div class="news-getintouch">
    <div class="wrap">
        <div class="news-part">
            <h2>From The News</h2>
            <h6>Keep on track of latest news and updates</h6>
            <ul>
                <li>
                    <h4>Lorem Ipsum Dolor Sit Amet Consectetuer Elit.</h4>
                    <div class="latest-news-disc">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur... <a href="#">Read More</a></div>
                </li>
                
                <li>
                    <h4>Lorem Ipsum Dolor Sit Amet Consectetuer Elit.</h4>
                    <div class="latest-news-disc">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur... <a href="#">Read More</a></div>
                </li>
                
                <li>
                    <h4>Lorem Ipsum Dolor Sit Amet Consectetuer Elit.</h4>
                    <div class="latest-news-disc">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur... <a href="#">Read More</a></div>
                </li>
                
            </ul>
        </div>
        <div class="getintouch" id="getintouch">
            <h2>Get In Touch</h2>
            <h6>Fill up your custom Requirements in the below form</h6>
            {{Former::horizontal_open()->action(URL::to("contact-us"))->method('post') }}
            {{ Former::hidden('ip','')->value(Request::getClientIp())}}
            <div class="getintouch-form">
              <p>{{ Former::text('name','')->placeholder('Full Name') }}</p>
                <p>{{ Former::text('email','')->placeholder('Your Email') }}</p>
                <p>{{ Former::text('phone','')->placeholder('Your Phone') }}</p>
                <p>{{ Former::textarea('message','')->placeholder('Type Message') }}</p>
                {{ Former::submit('Send Here')->class('getintouch-btn') }}
            </div>
            {{ Former::close() }}
        </div>
        <div class="c"></div>
    </div>
</div>

@stop