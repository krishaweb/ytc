@section('title')
	For Prioviders
@stop

@section('content')
<div class="banner-inner">
	<h1>For Providers</h1>
</div>

<div class="for-providers-page">
	<div class="wrap">
   	  <div class="for-providers-page-left">
        	<h2>Lorem ipsum dolor sit amet, consectetuer</h2>
        <p>Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.  Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.  Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            
          <ul>
           	  <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
           	  <li>Cum sociis natoque penatibus et magnis dis parturient </li>
           	  <li>Nulla consequat massa quis enim. </li>
           	  <li>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. </li>
           	  <li>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. </li>
           	  <li>Cum sociis natoque penatibus et magnis dis parturient </li>
           	  <li>Nulla consequat massa quis enim. </li>
          </ul>
            
        </div>
    	<div class="for-providers-page-right"><img src="{{asset('img/for-providers.jpg')}}"></div>
    	<div class="c"></div>

    
    <div class="for-providers-page-signup">
    	<div class="for-providers-page-signup-text">Sign Up. To be found. It's Free and Easy</div>
        <div class="for-providers-page-signup-btn"><a href="{{URL::to('sign-up')}}">Sign Up Here</a></div>
        <div class="c"></div>
    </div>
    
</div>
    </div>

@stop