@section('title')
{{ 'Thank You' }}
@stop
@section('content')
	<div class="thank-you" style="height: 373px;">
        <div class="wrap">
            <div class="thank-you-text">
            <h1>Thank You!</h1>
            Thanks for reaching out, <span>Jeremy</span>. I will get back to you soon.
            </div>
        </div>
    </div>
@stop