@section('title')
{{ 'Privacy Policy' }}
@stop

@section('content')
	<section class="inner-title">
        <div class="wrap">
            <h1>{{$page->title}}</h1>
            <div class="breadcumb"><a href="{{URL::to('/')}}">Home</a>  <span>/</span> {{$page->title}}</div>
            <div class="c"></div>
        </div>
    </section>
    
<section class="privacy-policy">
    <div class="wrap">
         {{$page->description}}
    </div>
</section>
    @include('shared.blog_whatson')
@stop