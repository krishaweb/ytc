@section('title')
	Resources
@stop

@section('content')
	<div class="banner-inner">
	<h1>Resources</h1>
</div>

<div class="resources-page">
	<div class="wrap">
    	<ul>
    	@foreach($resources as $r)
    		@if($r->type == 'pdf')
	        	<li class="pdf-download">
	            	<div class="resources-img"></div>
	                <div class="resources-text">{{$r->title}}</div>
	                <div class="resources-btn"><a href='{{ URL::to("download/{$r->pdf}") }}'></a></div>
	            </li>
            @else
	        	<li class="website-link">
	            	<div class="resources-img"></div>
	                <div class="resources-text">{{$r->title}}</div>
	                <div class="resources-btn"><a href="{{$r->link}}" target="_blank"></a></div>
	            </li>
	        @endif
        @endforeach
	        @if(count($resources) == 0)
	        	<h3> No resources found. </h3>
	        @endif
        </ul>
        <div class="c"></div>
    </div>
</div>

<div class="resources-pagination">
    <div class="wrap"><div class="pagination">
            	{{$resources->links()}}
            </div></div>
</div>
@stop