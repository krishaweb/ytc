@extends('layouts.base')
@section('title')
    404 Not Found Page
@stop

@section('content')
	<div class="thank-you" style="height: 373px;">
        <div class="wrap">
            <div class="thank-you-text page-not-found">
            <h1>Sorry, we couldn’t find what you were looking for...</h1>
            Return to our <a href="{{URL::to('/')}}">front page</a> or <a href="#">drop us a line</a> if you can’t find<br>what you were looking for.
            </div>
        </div>
    </div>
@stop