<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/admin">Your Training Choice</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
            <a  class="dropdown-toggle" data-toggle="dropdown">Course <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>{{ HTML::link('/admin/course_categories','Categories') }}</li>
              <li>{{ HTML::link('/admin/courses','Courses') }}</li>
            </ul> 
          </li>   
        <li class="dropdown">
          <a  class="dropdown-toggle" data-toggle="dropdown">News <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li>{{ HTML::link('/admin/news_categories','Categories') }}</li>
            <li>{{ HTML::link('/admin/news','News') }}</li>
          </ul> 
        </li>      
        <li class="dropdown">
          <a  class="dropdown-toggle" data-toggle="dropdown">Tools <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li>{{ link_to_route('admin.contents.index', 'Contents') }}</li>
            <li>{{ link_to_route('admin.subscribes.index', 'Subscribes') }}</li>
            <li>{{ link_to_route('admin.inquiries.index', 'Inquiries') }}</li> 
          </ul> 
        </li>
        <li>{{ HTML::link('/admin/resources','Resources') }}</li>
        <li>{{ link_to('admin/settings', 'Settings') }}</li>
      </ul>  
      <ul class="nav" style="margin-top:7px">
      <li> @if(Auth::check())
      <div class="dropdown pull-right" >
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
         Hello, {{ Auth::user()->name }}
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="/admin/change-pass">Change Password</a></li>
          <li role="presentation" class="divider"></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="/admin/logout" data-confirm ='Are you sure you want to logout?'>Logout</a></li>
        </ul>
      </div>
      @else
      <li><a href="login">Login</a></li>
      @endif
</li>
      </ul>
     

    
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>