<div class="row-fluid">  
  <div class="span6">{{ $rows->links() }}</div>
  <div class="span5 paging-info">Showing page <strong>{{$rows->getCurrentPage()}}</strong> of <strong>{{$rows->getLastPage()}}</strong> having <strong>{{$rows->getTotal()}}</strong> records</div>
</div>