@section('content')
<!-- trial login -->
<div class="container-fluid">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body">

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                        <?= Former::open()->rules(array(
                        'email' => "required|email",
                        'password' => "required|min:6|max:20"))->class('form-horizontal')->role('form') ?>
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                                       
                                    	 <input id="login-username" class="form-control" name="username" type="text" value="{{Input::get('username')}}" />
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    	<input class="form-control" name="password" id="login-password" type="password" />
                                    </div>
                                    

                                
                            <div class="input-group">
                                      <div class="checkbox">
                                        <label>
                                          <input id="login-remember" name="remember" value="1" type="checkbox"> Remember me
                                        </label>
                                      </div>
                                    </div>


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls"> 
                                       <button type="submit" id="btn-login" class="btn btn-success">Login</button>                             
                                    </div>
                                </div>


                                 
                             <?= Former::close() ?>



                        </div>                     
                    </div>  
        </div>
         
@stop