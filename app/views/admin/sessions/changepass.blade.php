@extends('admin.layouts.base')
@section('title')
    Your Training Choice - Change Password
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        Change Password
        
      </div>
      <div class="panel-body">       
        @if(Session::has('error'))
            {{ trans(Session::get('reason')) }}
        @endif
            
            {{
            Former::horizontal_open()->rules(array(
                "old_password" => "required", "password" => "required", "password_confirmation" => "required"
            ))
            }}
            {{ Former::password('old_password','Old password') }}
            {{ Former::password('password','New password') }}
            {{ Former::password('password_confirmation','Password confirmation') }}

            


                <div class="login-bottom">
                {{ Former::actions()->small_success_submit('Submit')->small_link('Back',(Input::get('next')? Input::get('next'):"/")) }}
                <div class="c"></div>
                </div>
                {{ Former::close() }}
           
            </div>
        </div>
    </div>
</div>
@stop
