<!DOCTYPE html>
<html>
<head>
  <title>{{ Config::get('site.name') }} | @yield('title')</title>
  <!-- styles -->
  {{ HTML::style('/a/css/bootstrap.min.css') }}
  {{ HTML::style('/a/css/bootstrap-theme.min.css') }}
  {{ HTML::style('/a/css/bootstrap-select.min.css') }}
  {{ HTML::style('/a/css/admin.css') }}
  {{ HTML::style('/a/css/uploadify.css')}}
  {{ HTML::style('/a/css/timepicki.css')}}
  {{ HTML::style('/a/css/ui-lightness/jquery-ui.min.css') }}
  @yield('styles') 
  
  @include('admin.shared.vars')
  @yield('vars') 
</head>
<body>

  @include('admin.shared.header')
  @include('admin.shared.flash')

  <div class="container">
  @yield('content')
  </div>

  <!-- scripts -->
  {{ HTML::script('/a/js/jquery.min.js') }}
  {{ HTML::script('/a/js/jquery-ui.min.js') }}
  {{ HTML::script('/a/js/bootstrap.min.js') }}
  {{ HTML::script('/a/js/bootstrap-select.min.js') }}
  {{ HTML::script('/a/js/admin.js') }}
  {{ HTML::script('/a/js/jquery.uploadify-3.1.min.js') }}
  {{ HTML::script('/a/js/timepicki.js') }}
  {{ HTML::script('/a/ckeditor/ckeditor.js') }}
  {{ HTML::script('/a/ckfinder/ckfinder.js') }}

  @yield('scripts')

</body>
</html>
