<!DOCTYPE html>
<html>
<head>
  <title>Your Training Choice</title>
  <!-- styles -->
  {{ HTML::style('/a/css/bootstrap.min.css') }}
  {{ HTML::style('/a/css/bootstrap-theme.min.css') }}
  {{ HTML::style('/a/css/ui-lightness/jquery-ui.min.css') }}
  {{ HTML::style('/a/css/bootstrap-select.min.css') }}
  {{ HTML::style('/a/css/admin.css') }}
  {{ HTML::style('/a/css/uploadify.css')}}
  @yield('styles') 
  
  @include('admin.shared.vars')
  @yield('vars') 
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/admin">Your Training Choice</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">     
      <ul class="nav navbar-nav navbar-right">
        @if(Auth::check())
        <li class="has-dropdown">
          <a href="#">Hello, {{ Auth::user()->name }}</a>
          <ul class="dropdown">
            <li><a href="/admin/logout">Logout</a></li>
          </ul>
        </li>      
        @else
        <li><a href="/admin/login">Login</a></li>
        @endif
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  
  @include('admin.shared.flash')

  <div class="container">
  @yield('content')
  </div>

  <!-- scripts -->
  {{ HTML::script('/a/js/jquery.min.js') }}
  {{ HTML::script('/a/js/jquery-ui.min.js') }}
  {{ HTML::script('/a/js/bootstrap.min.js') }}
  {{ HTML::script('/a/js/bootstrap-select.min.js') }}
  {{ HTML::script('/a/js/admin.js') }}
  {{ HTML::script('/a/js/jquery.uploadify-3.1.min.js') }}
  {{ HTML::script('/a/ckeditor/ckeditor.js') }}
  {{ HTML::script('/a/ckfinder/ckfinder.js') }}

  @yield('scripts')

</body>
</html>
