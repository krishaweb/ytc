@extends('admin.layouts.base')
@section('title')
    Inquiries
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">

        Inquiries
          
   
      </div>
      <div class="panel-body">        
        @if(count($rows))
          <table class="table table-condensed table-striped" id="grid">
            <thead>
              <tr>
                <th class="text-right" width="50">ID</th>                
                <th>Name</th>        
                <th>Email</th> 
                <th>Subject</th>
                <th>Message</th>
                <th class="text-center" width="150">Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rows as $row)
              <tr data-id="{{$row->id}}">
                <td align="right">{{$row->id}}</td>
                
                <td>{{ $row->name }} </td>
                <td>{{ $row->email }} </td>
                <td>{{ $row->subject }} </td>
                <td>{{ $row->message }} </td>                
               
                <td align="center">
                  {{ link_to_route("admin.inquiries.show","Details",array($row->id),array('class'=>'btn btn-xs btn-default')) }}
                  {{ link_to_route("admin.inquiries.destroy","Delete",array($row->id),array('class'=>'btn btn-xs btn-danger','data-method'=>"delete")) }}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>          
          @include('admin.shared.paging')
        @else
          <p class="alert alert-warning">No results to display</p>
        @endif

      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="panel panel-default">
      <div class="panel-heading">Filters</div>
      <div class="panel-body">
        {{ Former::open_vertical()->method('GET') }}
        {{ Former::text('id')->label('ID') }}
        {{ Former::text('name') }}
        {{ Former::text('email') }}
        {{ Former::text('subject') }}
        {{ Former::actions()->success_submit('Filter')->small_link('Reset',URL::to('/admin/inquiries')) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>
</div>

@stop


