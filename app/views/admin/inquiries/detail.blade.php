@section('title')
	 Inquiries Detail 
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        Inquiry Detail
      </div>
      <div class="panel-body">        
        @if(count($rows))
          <table class="table table-condensed table-striped" id="grid">
            
            <tbody>

              <tr><td><h3>Inquiry Details:</h3></td><td></td></tr>

	              <tr>
	                  <td width="200"> Name:</td>
	                  <td>{{$rows->name?$rows->name : '-'}}</td>                 
	              </tr>
	             <tr>
	                  <td width="200"> Email:</td>
	                  <td>{{$rows->email?$rows->email : '-'}}</td>                 
	              </tr>
	              <tr>
	                  <td width="200">Subject:</td>
	                  <td>{{$rows->subject?$rows->subject : '-'}}</td>                 
	              </tr>
	               <tr>
	                  <td width="200">Message:</td>
	                  <td>{{$rows->message?$rows->message : '-'}}</td>                 
	              </tr>
	             
            </tbody>
          </table>          
         
        
        @endif

      </div>
    </div>
  </div>
  <div class="list-map">
  <div id="map" style="height: 275px;"></div>
  </div>
  
@stop



