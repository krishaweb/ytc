@section('title')
{{ 'Course Category' }}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
       Course Category    
      </div>
      <div class="panel-body">        
      
         @include("admin.shared.errors")
      <?=
        Former::horizontal_open()->rules(array(
          "name" => "required"
      ))
      ?>
        {{ Former::select('parent_id',"Parent")->options(Course_Category::lists('name','id')) }}
        {{ Former::text("name")->help("64 characters max") }}
        {{ Former::text("slug",'URL Key')->help("URL key of category, Automatically generated if not given") }}
        {{ Former::actions()->success_submit('Submit')->small_link('Back',(Input::get('next')? Input::get('next'):"/{$prefix}/{$controller}")) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>  
</div>
@stop
