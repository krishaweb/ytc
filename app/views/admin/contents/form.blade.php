@section('title')
   {{'Content'}}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
      {{$id?'Edit Content':'Add Content'}}
      </div>
      <div class="panel-body">        
          {{
            Former::horizontal_open()->rules(array(
            'name' => 'required'
            ))->action($id? URL::route("admin.contents.update",array($id)) : URL::route("admin.contents.store") )
            ->method($id? 'put':'post')

          }}
        {{ Former::text("code") }}
        {{ Former::text("title") }}
        {{ Former::textarea("description")->id('description') }}
        {{ Former::text("meta_title") }}
        {{ Former::text("meta_keywords") }}
        {{ Former::textarea("meta_description")->id('meta_description') }} 
        {{ Former::actions()->small_success_submit('Submit')->small_link('Back',(Input::get('next')? Input::get('next'):"/admin/contents")) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>  
</div>
@stop

@section('scripts')
<script>

$(document).ready(function() {
CKFinder.setupCKEditor(CKEDITOR.replace( 'description' ),{ basePath: "{{URL::asset('/ckfinder')}}" });
CKFinder.setupCKEditor(CKEDITOR.replace( 'meta_description' ),{ basePath: "{{URL::asset('/ckfinder')}}" });

});
</script>

@stop