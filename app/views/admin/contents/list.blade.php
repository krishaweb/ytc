@extends('admin.layouts.base')
@section('title')
    Contents
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">

        Contents
          {{ link_to_route('admin.contents.create',"Add",array(),array('class'=>'pull-right btn btn-xs btn-primary')) }}
   
      </div>
      <div class="panel-body">        
        @if(count($rows))
          <table class="table table-condensed table-striped" id="grid">
            <thead>
              <tr>
                <th class="text-right" width="50">ID</th>                
                <th>Code</th>        
                <th>Title</th> 
                <th>Meta Title</th>
                <th>Meta Keywords</th>
                <th class="text-center" width="150">Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rows as $row)
              <tr data-id="{{$row->id}}">
                <td align="right">{{$row->id}}</td>
                
                <td>{{ $row->code }} </td>
                <td>{{ $row->title }} </td>
                <td>{{ $row->meta_title }} </td>
                <td>{{ $row->meta_keywords }} </td>                
               
                <td align="center">
                  {{ link_to_route("admin.contents.edit","Edit",array($row->id), array('class'=>'btn btn-xs btn-primary')) }}
                  {{ link_to_route("admin.contents.destroy","Delete",array($row->id),array('class'=>'btn btn-xs btn-danger','data-method'=>"delete")) }}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>          
          @include('admin.shared.paging')
        @else
          <p class="alert alert-warning">No results to display</p>
        @endif

      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="panel panel-default">
      <div class="panel-heading">Filters</div>
      <div class="panel-body">
        {{ Former::open_vertical()->method('GET') }}
        {{ Former::text('id')->label('ID') }}
        {{ Former::text('code') }}
        {{ Former::text('title') }}
        {{ Former::text('meta_title') }}
        {{ Former::text('meta_keyword') }}
        {{ Former::actions()->success_submit('Filter')->small_link('Reset',URL::to('/admin/contents')) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>
</div>

@stop


