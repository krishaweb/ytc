@section('title')
{{ 'Resources' }}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        Resources
        {{ link_to_route('admin.resources.create',"Add",array(),array('class'=>'pull-right btn btn-xs btn-primary')) }}
      </div>
      <div class="panel-body">        
        @if(count($rows))
          <table class="table table-condensed table-striped" id="grid">
            <thead>
              <tr>
                <th width="30">Id</th>
                <th>Title</th>
                <th width="50">Type</th>
                <th width="120">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rows as $row)
              <tr data-id="{{$row->id}}">
                <td align="right">{{$row->id}}</td>
                <td>{{$row->title}}</td>                                                                
                <td>{{$row->type}}</td>
                <td align="center">
                  
                  {{ link_to_route("admin.resources.edit","Edit",array($row->id), array('class'=>'btn btn-xs btn-primary')) }}
                  {{ link_to_route("admin.resources.destroy","Delete",array($row->id), array('class'=>'btn btn-xs btn-danger','data-method'=>'delete')) }}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>          
          @include('admin.shared.paging')
        @else
          <p class="alert alert-warning">No results to display</p>
        @endif

      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="panel panel-default">
      <div class="panel-heading">Filters</div>
      <div class="panel-body">
        {{ Former::open_vertical()->method('GET') }}
        {{ Former::text("id")->label("ID") }}      
        {{ Former::text("title","Title") }}
        {{ Former::select("type","Published")->options( array(''=>'-- ANY --','1'=>'Yes','0'=>'No') )  }}
        {{ Former::actions()->success_submit('Filter')->small_link('Reset',URL::current()) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>
</div>
@stop





