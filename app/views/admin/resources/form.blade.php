@section('title')
{{ 'Resource' }}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
       Resource   
      </div>
      <div class="panel-body">        
      
         @include("admin.shared.errors")
          {{
            Former::horizontal_open()->action($edit? URL::route("admin.resources.update",array($id)) : URL::route("admin.resources.store") )->method($edit? 'put':'post')
          }}
          {{ Former::text("title")->help("255 characters max") }}
          {{ Former::radio('type','')->radios(assoc_to_radios(Resource::$types,'type'))->inline()->check('pdf')->label("Select Resource type")->class('type') }}
        <div id="pdf">
           
        {{ Former::hidden('pdf')->id('photo') }}
        {{ Former::file('pdf_upload','PDF') }}
        </div>
        <div id="link">
          {{ Former::url("link")->help("255 characters max") }}  
        </div>  
        <div class="col-sm-offset-2 col-sm-10"> 
          {{ Form::hidden('status',0) }}
          {{ Former::checkbox('status','')->value(1)->text('Show this resource on website?')->check() }}
        </div>      
        {{ Former::actions()->success_submit('Submit')->small_link('Back',(Input::get('next')? Input::get('next'):"/{$prefix}/{$controller}")) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>  
</div>

@stop

@section('scripts')
<script>
$(document).ready(function() {
  var type = '';
  $('#link').hide();
  $('#pdf').show();
  $('input[type=radio][name=type]').click(function(){

    if(document.getElementById('type_pdf').checked)
    {
      type = document.getElementById('type_pdf').value;
    }
    else{
      type = document.getElementById('type_link').value;
    }
    
    if(type=="pdf"){
      $('#link').hide();
      $('#pdf').show();
    }
    else{
      $('#pdf').hide();
      $('#link').show();
    }

  });
  

  $('#pdf_upload').uploadify({
        'multi': false,
        'auto': true,
        'fileTypeDesc' : 'PDF File',
        'fileTypeExts' : '*.pdf',
        'cancelImg': '{{ asset("/a/misc/uploadify-cancel.png") }}',
        'swf': '{{ asset("/a/misc/uploadify.swf") }}',
        'uploader': '{{ asset("/a/misc/uploadify.php") }}',
        onUploadSuccess: function(file, data, response) {
        $('#photo').val(data);
        }
  });
});
</script>

@stop