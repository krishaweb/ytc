@section('title')
{{ 'News Categories' }}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        News Categories
        {{ link_to('/admin/news_categories/add',"Add",array('class'=>'pull-right btn btn-xs btn-primary')) }}
      </div>
      <div class="panel-body">        
        @if(count($rows))
          <table class="table table-condensed table-striped" id="grid">
            <thead>
              <tr>
                <th class="text-right" width="50">ID</th>   
                <th> Name </th>                      
                <th class="text-center" width="150">Actions</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rows as $row)
              <tr data-id="{{$row->id}}">
                <td align="right">{{$row->id}}</td>
                <td>{{$row->name}}</td>                                                                
                <td align="center">
                  {{ HTML::link("/{$prefix}/{$controller}/edit/{$row->id}?next=".urlencode(URL::full()),"Edit",array("class"=>"btn btn-xs btn-primary")) }}
                  {{ HTML::link("/{$prefix}/{$controller}/delete/{$row->id}","Delete",array('class'=>'btn btn-xs btn-danger','data-confirm'=>"Are you sure?")) }}
                  
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>          
          @include('admin.shared.paging')
        @else
          <p class="alert alert-warning">No results to display</p>
        @endif

      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="panel panel-default">
      <div class="panel-heading">Filters</div>
      <div class="panel-body">
        {{ Former::open_vertical()->method('GET') }}
        {{ Former::text("id")->label("ID") }}
        {{ Former::text("name","Name") }}
        {{ Former::actions()->success_submit('Filter')->small_link('Reset',URL::current()) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>
</div>
@stop





