@section('title')
{{ 'News' }}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
       News    
      </div>
      <div class="panel-body">        
      
         @include("admin.shared.errors")
      {{
            Former::horizontal_open()->action($edit? URL::route("admin.news.update",array($id)) : URL::route("admin.news.store") )->method($edit? 'put':'post')
          }}
        {{ Former::text("title")->help("255 characters max") }}
        <div class="row">
        <label class="col-md-2 text-right">Photo</label>
        <div class="controls">
        <img src="{{$row->photo_url('thumb')}}" id="photo_preview" class="img-thumbnail" height="64" width="64" style="margin-left:11px" />
        <a href="javascript:;" id="photo_remove" class="btn btn-danger btn-mini"><i class="icon icon-remove"></i> Remove current</a>
        </div>
        </div> <br/> 
        {{ Former::hidden('photo')->id('photo') }}
        {{ Former::file('photo_upload','Photo') }}
        {{ Former::text("date")->value(date('Y-m-d'))->help("Select date to be shown as publish date")->class('span2') }}
        <div class="col-sm-offset-2 col-sm-10"> 
          {{ Form::hidden('status',0) }}
          {{ Former::checkbox('status','')->value(1)->text('Show this news on website?')->check() }}
        </div>      
        {{ Former::textarea("intro",'Introduction Text')->id('intro')->help('This will be displayed on blog list page') }}          
        {{ Former::textarea("content")->id('Full Text')->help('This will be displayed on blog details page') }}      
        {{ Former::text("meta_title")->label("Meta Title")->help("255 characters max")->class('col-xs-4') }}
        {{ Former::text("meta_keywords")->label("Meta Keywords")->help("255 characters max")->class('col-xs-4') }}
        {{ Former::text("meta_description")->label("Meta Description")->help("255 characters max")->class('col-xs-4') }}
        {{ Former::actions()->success_submit('Submit')->small_link('Back',(Input::get('next')? Input::get('next'):"/{$prefix}/{$controller}")) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>  
</div>

@stop

@section('scripts')
<script>

$(document).ready(function() {
  CKFinder.setupCKEditor(CKEDITOR.replace( 'intro' ),{ basePath: "{{URL::asset('/a/ckfinder')}}" });
  CKFinder.setupCKEditor(CKEDITOR.replace( 'content' ),{ basePath: "{{URL::asset('/a/ckfinder')}}" });

  $('#date').datepicker({dateFormat:'yy-mm-dd'});
  $('#photo_remove').click(function() {

      if (confirm('Are you sure?')) 
      {
        $('#photo_preview').attr('src', 'http://placehold.it/64x64');
        $('#photo').val('');
      }

});

  $('#photo_upload').uploadify({
        'multi': false,
        'auto': true,
        'swf': '{{ asset("/a/misc/uploadify.swf") }}',
        'uploader': '{{ asset("/a/misc/uploadify.php") }}',
        onUploadSuccess: function(file, data, response) {
        $('#photo_preview').attr('src', app.tmp_url + data);
        $('#photo').val(data);

        }
  });
});
</script>

@stop