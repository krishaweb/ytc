@section('title')
{{ 'Course' }}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
       Course    
      </div>
      <div class="panel-body">        
      
         @include("admin.shared.errors")
      {{
            Former::horizontal_open()->action($edit? URL::route("admin.courses.update",array($id)) : URL::route("admin.courses.store") )->method($edit? 'put':'post')
          }}
        {{ Former::select("category_id","Category")->options( Course_Category::categories('-- NONE --') ) }}
        {{ Former::hidden('provider_id')->value(Auth::user()->id) }}            
        {{ Former::text("title")->help("255 characters max") }}
        <div class="row">
        <label class="col-md-2 text-right">Photo</label>
        <div class="controls">
        <img src="{{$row->photo_url('thumb')}}" id="photo_preview" class="img-thumbnail" height="64" width="64" style="margin-left:11px" />
        <a href="javascript:;" id="photo_remove" class="btn btn-danger btn-mini"><i class="icon icon-remove"></i> Remove current</a>
        </div>
        </div> <br/> 
        {{ Former::hidden('photo')->id('photo') }}
        {{ Former::file('photo_upload','Photo') }}
        {{ Former::textarea("description")->id('description') }}
        {{ Former::text("country") }}
        {{ Former::text("location") }}
        {{ Former::text("profession") }}
        {{ Former::text("level") }}
        {{ Former::text("accreditation") }}
        {{ Former::text("sector") }}
        {{ Former::text("delivery_method") }}
        {{ Former::text("subject") }}
        {{ Former::text("course_date")->value(date('Y-m-d'))->class('span2') }}
        {{ Former::text("price") }}
        <div class="col-sm-offset-2 col-sm-10"> 
          {{ Form::hidden('status',0) }}
          {{ Former::checkbox('status','')->value(1)->text('Show this Course on website?')->check() }}
        </div>      
        {{ Former::actions()->success_submit('Submit')->small_link('Back',(Input::get('next')? Input::get('next'):"/{$prefix}/{$controller}")) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>  
</div>

@stop

@section('scripts')
<script>

$(document).ready(function() {
  CKFinder.setupCKEditor(CKEDITOR.replace( 'description' ),{ basePath: "{{URL::asset('/a/ckfinder')}}" });

  $('#course_date').datepicker({dateFormat:'yy-mm-dd'});
  $('#photo_remove').click(function() {

      if (confirm('Are you sure?')) 
      {
        $('#photo_preview').attr('src', 'http://placehold.it/64x64');
        $('#photo').val('');
      }

});

  $('#photo_upload').uploadify({
        'multi': false,
        'auto': true,
        'swf': '{{ asset("/a/misc/uploadify.swf") }}',
        'uploader': '{{ asset("/a/misc/uploadify.php") }}',
        onUploadSuccess: function(file, data, response) {
        $('#photo_preview').attr('src', app.tmp_url + data);
        $('#photo').val(data);

        }
  });
});
</script>

@stop