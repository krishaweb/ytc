@section('title')
{{ 'Courses' }}
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        Courses
        {{ link_to_route('admin.courses.create',"Add",array(),array('class'=>'pull-right btn btn-xs btn-primary')) }}
      </div>
      <div class="panel-body">        
        @if(count($rows))
          <table class="table table-condensed table-striped" id="grid">
            <thead>
              <tr>
                <th width="30">Id</th>
                <th width="100">Category</th>
                <th width="100">Provider</th>            
                <th>Title</th>
                <th width="50">Status</th>
                <th width="120">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rows as $row)
              <tr data-id="{{$row->id}}">
                <td align="right">{{$row->id}}</td>
                <td>{{ $row->category? $row->category->name : '-' }}</td>        
                <td>{{ $row->provider? $row->provider->name : '-' }}</td>        
                <td>{{$row->title}}</td>                                                                
                <td>
                @if($row->status)
                  <span class="label label-success">Active</span>
                @else
                  <span class="label label-warning">Inactive</span>
                @endif
                </td>
                <td align="center">
                  
                  {{ link_to_route("admin.courses.edit","Edit",array($row->id), array('class'=>'btn btn-xs btn-primary')) }}
                  {{ link_to_route("admin.courses.destroy","Delete",array($row->id), array('class'=>'btn btn-xs btn-danger','data-method'=>'delete')) }}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>          
          @include('admin.shared.paging')
        @else
          <p class="alert alert-warning">No results to display</p>
        @endif

      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="panel panel-default">
      <div class="panel-heading">Filters</div>
      <div class="panel-body">
        {{ Former::open_vertical()->method('GET') }}
        {{ Former::text("id")->label("ID") }}      
        {{ Former::select("category_id","Category")->options( Course_Category::categories('-- ANY --') )  }}
        {{ Former::text("title","Title") }}
        {{ Former::select("status","Status")->options( array(''=>'-- ANY --','1'=>'Active','0'=>'Inactive') )  }}
        {{ Former::actions()->success_submit('Filter')->small_link('Reset',URL::current()) }}
        {{ Former::close() }}
      </div>
    </div>
  </div>
</div>
@stop





