@section('title')
	 Newsletter Subscription Detail 
@stop
@section('content')
<div class="row"> 
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        Subscription Detail
      </div>
      <div class="panel-body">        
        @if(count($rows))
          <table class="table table-condensed table-striped" id="grid">
            
            <tbody>

              <tr><td colspan="2"><h3>Subscription Details:</h3></td></tr>
	             <tr>
	                  <td width="200"> Email:</td>
	                  <td>{{$rows->email?$rows->email : '-'}}</td>                 
	              </tr>
	              <tr>
	                  <td width="200">IP:</td>
	                  <td>{{$rows->ip?$rows->ip : '-'}}</td>                 
	              </tr>
	               <tr>
	                  <td width="200">User Agent:</td>
	                  <td>{{$rows->ua?$rows->ua : '-'}}</td>                 
	              </tr>
                <tr>
                  <td width="200">Created at:</td>
                  <td>{{ $rows->created_at->format('j F Y H:i:s') }}</td>
                </tr>
	             
            </tbody>
          </table>          
         
        
        @endif

      </div>
    </div>
  </div>
  <div class="list-map">
  <div id="map" style="height: 275px;"></div>
  </div>
  
@stop



