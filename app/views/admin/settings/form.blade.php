@extends('admin.layouts.base')
@section('title')
    Settings
@stop
@section('content')
<div class="row-fluid">
    <div class="box span12">
        <div class="box-header well">
            <h2>Settings</h2>     
        </div>
        <div class="box-content">
            @include("{$prefix}.shared.errors")
            <?=
            Former::horizontal_open()
            ?>

            <fieldset>
                <legend>General Settings</legend>
                {{ Former::text("site_name","Site Name",Config::get('site.name'))->help('This name is displayed at various places.') }}
                {{ Former::text("site_email","Site Email",Config::get('site.email'))->help('Email address to be displayed') }}
                {{ Former::text("site_phone","Site Phone",Config::get('site.phone'))->help('Phone number to displayed') }}       
                {{ Former::text("site_fax","Site Fax",Config::get('site.fax'))->help('Fax number to displayed') }}       
            </fieldset>

            <fielset>
                <legend>Social Media</legend>
                {{ Former::text("site_facebook_url","Facebook Url",Config::get('site.facebook.url')) }}
                {{ Former::text("site_twitter_url","Twitter Url",Config::get('site.twitter.url')) }}
                {{ Former::text("site_googleplus_url","Google Plus Url",Config::get('site.googleplus.url')) }}
            </fielset>

            <fielset>
                <legend>Search Engine Optimization</legend>
                {{ Former::text("site_title","Meta Title",Config::get('site.title'))->class('span6')->help('80 characters') }}
                {{ Former::textarea("site_keywords","Meta Keywords",Config::get('site.keywords'))->help('120 characters')->class('span6') }}
                {{ Former::textarea("site_description","Meta Description",Config::get('site.description'))->help('255 characters')->class('span6') }}
            </fielset>


            {{ Former::actions()->small_success_submit('Save')->small_link('Back',(Input::get('next')? Input::get('next'):"/admin/")) }}

            {{ Former::close() }}
        </div>
    </div> 
</div>
@stop
