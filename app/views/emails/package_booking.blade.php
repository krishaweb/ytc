<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Package Booking Order Detail</title>
<style type="text/css">
body{
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
</style>
</head>

<body>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:20px; margin-bottom:20px; padding:20px; padding-top:10px; padding-bottom:10px; border:#e6e6e6 solid 1px;">
  <tr>
    <td align="left" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="0" style="border-bottom:#666666 solid 1px;">
      <tr>
        <td width="50%" align="left" valign="top"><img src="{{asset('img/logo.png')}}" width="267" height="119" /></td>
        <td align="right" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:23px; color:#333333; padding-top:35px;">Date: <?php
$date = new DateTime();
echo $date->format('d M Y : H:i A');
?> <br />
          Phone: (03) 6178 4627<br />
          Email: <a href="mailto:info@platinumluxurycars.com" style="color:#333333; text-decoration:none;">info@platinumluxurycars.com</a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top" style="font-size:24px; color:#cb181e; font-family:Arial, Helvetica, sans-serif; padding:20px; padding-top:25px;">Booking Detail</td>
  </tr>
  <tr>
    <td align="center" valign="top" style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:#333333; padding-bottom:20px;">Thank you for choosing <a href="#" style="color:#333333; text-decoration:none;">Platinumluxarycars.com.au</a></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-bottom:20px;">
    <table width="600" border="0" cellspacing="0" cellpadding="0" style="font-size:14px; color:#333333; font-family:Arial, Helvetica, sans-serif; ">
      <tr>
        <td width="150" align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;"><strong>Order Id:</strong></td>
        <td width="30" align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;"><strong>{{$id}}</strong></td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">Name: </td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;">{{$name}}</td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">E-mail:</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;"><a href="mailto:{{$email}}" style="color:#333333; text-decoration:none;">{{$email}}</a></td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">Phone </td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;">{{$phone?$phone:'-'}}</td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">Package:</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;">{{$package}}</td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">Date &amp; Time:</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;">
          <?php
$date = new DateTime($paid_on);
echo $date->format('d M Y : H:i A');
?>
        </td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">Payment Type:</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;">{{$payment_type}}</td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">Transaction Id#:</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;">{{$transaction_id}}</td>
      </tr>
      <tr>
        <td align="right" valign="top" style="padding-top:7px; padding-bottom:7px; color:#000000;">Price</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" style="padding-top:7px; padding-bottom:7px;">${{number_format($amount)}}</td>
      </tr>
      
    </table></td>
  </tr>
  
  <tr>
    <td align="center" valign="top" style="padding-top:20px; padding-bottom:20px; border-top:#e6e6e6 solid 1px; font-size:20px; color:#cb181e; font-family:Arial, Helvetica, sans-serif;">Thank You!</td>
  </tr>
  
</table>
</body>
</html>
