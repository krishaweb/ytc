<!DOCTYPE html>

<html lang="en-US">

	<head>
	<meta charset="utf-8">
	</head>

	<body>

		<h2>Activation link</h2>

		<p>Hi <?= Input::get('name') ?>,</p>
			<p>Please click the link below to activate your account.</p>
		<div>
			{{ URL::Route('activate', array($reset_token)) }}.
		</div>

	</body>

</html>
