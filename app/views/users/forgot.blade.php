@section('content')
<div class="content">
<div class="c"></div>
	<div class="content">
		<div class="inner-page">
		@if (Session::has('error'))
		{{ trans(Session::get('reason')) }}
		@elseif (Session::has('success'))
		An e-mail with the password reset has been sent.
		@endif
			<div class="forgot">
			{{
			Former::open()->rules(array(
			'email' => 'required|email'
			))
			}}
				<div class="login-form">
				<h3>Send reset password instructions</h3>
				<div class="signup-email">
				<div class="signup-email-icon"></div>
				{{ Former::text('email')->placeholder('Mail')->label(false) }}
				</div>

				</div>
				<div class="login-bottom">
				{{ Former::actions()->success_submit('Submit') }}
				<div class="c"></div>
				</div>
			{{ Former::close() }}
			</div>
		</div>
	<div class="c"></div>
	</div>
</div>
@stop

