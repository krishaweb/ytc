@section('title')
  Sign Up
@stop
@section('content')
<div class="banner-inner">
  <h1>Sign Up</h1>
</div>
<div class="signup">
  <div class="wrap">
    <div class="signup-form">
      <h2>Sign Up
       <span class="or-login">or <a href="{{URL::to('login')}}">Login</a></span>
      </h2>
      {{Former::horizontal_open()->action(URL::to("sign-up"))->method('post') }}
      <div>
        <div class="signup-form-left">
       {{ Former::text('fname')->placeholder('First Name')->label(false) }}
        </div>
        <div class="signup-form-right">
          {{ Former::text('lname')->placeholder('Last Name')->label(false) }}
        </div>
      </div>
      <div class="c"></div>
      <p>{{ Former::text('email')->placeholder('Email')->label(false) }}</p>
      <p>{{ Former::password('password')->placeholder('Password')->label(false) }}</p>
      <p>{{ Former::password('password_confirmation')->placeholder('Repeat Password')->label(false) }}</p>
      <div class="login-btn-part">
          {{ Former::submit('Sign Up')->class('signup-btn') }}
          {{ Form::hidden('status',0) }}
          {{ Former::checkbox('subscribed','')->value(1)->text('Subscribe To Our Newsletter?') }}
            <div class="c"></div>
      </div>
      {{ Former::close() }}
    </div>
  </div>
</div>

@stop

      