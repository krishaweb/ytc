@section('title')
    Login
@stop

@section('content')
<div class="banner-inner">
  <h1>Login</h1>
</div>
@include('shared.flash')
<div class="signup">
  <div class="wrap">
    <div class="signup-form login-form">
      <h2>Login
       <span class="or-login">or <a href="{{URL::to('sign-up')}}">Sign Up</a></span>
      </h2>
      {{ Former::horizontal_open()->action(URL::to('/login'))->method('post') }}
      <div>
        {{ Former::text('email')->label(false)->placeholder('User Name') }}
        <p>{{ Former::password('password')->label(false)->placeholder('Password') }}</p>
        {{ Former::checkbox('remember','')->value(1)->text('Remember me') }}
        <div class="login-btn-part">
            {{ Former::submit('Login')->class('signup-btn') }}
            {{ link_to('/forgot-password',"Forgot your password?") }}
            <div class="c"></div>
        </div>
      </div>
      {{ Former::close() }}
    </div>
  </div>
</div>
@stop