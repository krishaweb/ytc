@section('title')
	Change Password
@stop

@section('content')
	<div class="banner-inner">
  <h1>My Account</h1>
</div>
@include('shared/flash')
<div class="my-account">
	<div class="wrap">
		<div class="my-account-left">
        	<div class="my-account-menu">
            	<h4>My Account</h4>
                <ul>
                	<li><a href="{{URL::to('my/courses')}}">My Course</a></li>
                	<li><a href="#">Create Course</a></li>
                	<li><a href="{{URL::to('my/edit-profile')}}">Edit Profile</a></li>
                	<li><a href="{{URL::to('my/change-password')}}" class="{{Request::segment('2') == 'change-password' ? 'active' : ''}}">Change Password</a></li>
                	<li><a href="{{URL::to('logout')}}">Log out</a></li>
                </ul>
            </div>
            
            <div class="need-help">
            	<h4>Need Help?</h4>
                <div class="need-help-disc">If our options do not match your course or you have any special requirements please get in touch with us today.</div>
                <div class="need-help-email-us"><a href="mailto:Courses@yourtrainingchoice.com.au">Email Us</a></div>
            </div>
            
        </div>
		<div class="my-account-right">
        	<h2>Change Password</h2>
          {{Former::horizontal_open()->action(URL::to("my/change-password"))->method('post') }}
          <div class="create-course-form">
            	<p>{{ Former::password('old_password','')->placeholder('Old Password')->label(false) }}</p>
            	<p>{{ Former::password('password')->placeholder('New Password')->label(false) }}</p>
            	<p>{{ Former::password('password_confirmation')->placeholder('Confirm New Password')->label(false) }}</p>
              <div>
              	{{ Former::submit('Save Password')->class('submit-course-btn') }}
              	<div class="c"></div>
              </div>
              
          </div>
          {{ Former::close() }}
            
        </div>
		<div class="c"></div>
    </div>
</div>
@stop