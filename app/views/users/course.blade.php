@section('title')
	My Course
@stop

@section('content')
	<div class="banner-inner">
  <h1>My Account</h1>
</div>
@include('shared/flash')
<div class="my-account">
	<div class="wrap">
		<div class="my-account-left">
        	<div class="my-account-menu">
            	<h4>My Account</h4>
                <ul>
                	<li><a href="{{URL::to('/my/courses')}}" class="{{Request::segment('2') == 'courses' ? 'active' : ''}}">My Course</a></li>
                	<li><a href="create-course.php">Create Course</a></li>
                	<li><a href="{{URL::to('my/edit-profile')}}">Edit Profile</a></li>
                	<li><a href="{{URL::to('my/change-password')}}">Change Password</a></li>
                	<li><a href="{{URL::to('logout')}}">Log out</a></li>
                </ul>
            </div>
            
            
        </div>
		<div class="my-account-right">
        	<h2>My Course</h2>
            
            <div class="choose-training-listing">
            	<ul>
            	@if(count($courses) > 0)	
            		@foreach($courses as $c)
	                	<li>
	                    	<div class="choose-training-listing-left">
		                    	<h3><a href="#">Lorem Ipsum Dolor Sit Amet Consectetuer Adipiscing.</a></h3>
	                          <div class="choose-training-listing-disc">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur </div>
	                            <div class="choose-training-listing-left-icon">
	                       	    <img src="{{asset('img/country-icon.png')}}">
	                            <img src="{{asset('img/location-icon.png')}}">
	                            <img src="{{asset('img/profetion-icon.png')}}">
	                            <img src="{{asset('img/level-icon.png')}}">
	                            <img src="{{asset('img/accreditation-icon.png')}}">
	                            <img src="{{asset('img/sector-icon.png')}}">
	                            <img src="{{asset('img/delivery-method-icon.png')}}">
	                            <img src="{{asset('img/subject-icon.png')}}">
	                            <img src="{{asset('img/course-date.png')}}">
	                            <img src="{{asset('img/price-icon.png')}}"> </div>
	                      </div>
	                        
	                    	<div class="choose-training-listing-right">
	                        	<div class="choose-training-listing-img"><img src="{{asset('img/choose-training-list-img.jpg')}}"></div>
	                            <div class="choose-training-listing-price">$10,000</div>
	                            <div class="view-course-btn"><a href="#">View Course</a></div>
	                      	</div>
	                    	<div class="c"></div>
	                    </li>
                    @endforeach
                @else
                	<h3> No courses found. </h3>
                @endif
                </ul>
            </div>
            
			<div class="pagination">
            	{{ $courses->links() }}
            </div>
            
            
        </div>
		<div class="c"></div>
    </div>
</div>
@stop