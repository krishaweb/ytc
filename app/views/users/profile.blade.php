@section('title')
	Edit Profile
@stop

@section('content')
<div class="banner-inner">
  <h1>My Account</h1>
</div>
@include('shared/flash')
<div class="my-account">
	<div class="wrap">
		<div class="my-account-left">
        	<div class="my-account-menu">
            	<h4>My Account</h4>
                <ul>
                	<li><a href="{{URL::to('my/courses')}}">My Course</a></li>
                	<li><a href="#">Create Course</a></li>
                	<li><a href="{{URL::to('my/edit-profile')}}" class="{{ Request::segment('2') == 'edit-profile' ? 'active' : '' }}">Edit Profile</a></li>
                	<li><a href="{{URL::to('my/change-password')}}">Change Password</a></li>
                	<li><a href="{{URL::to('logout')}}">Log out</a></li>
                </ul>
            </div>
            
        </div>
		<div class="my-account-right">
        	<h2>Edit Profile</h2>
          {{Former::horizontal_open()->action(URL::to("my/edit-profile"))->method('post') }}
          <div class="create-course-form">
        
                	{{ Former::text('name','')->label('Provider Name') }}
                
            	<p>
                	{{ Former::text('email','')->label('Provider Email Address') }}
                </p>
                
            	<p>
                	{{ Former::text('phone','')->label('Contact Number') }}
                </p>
                
            	<p>
                	{{ Former::text('url','')->label('Url') }}
                </p>

                <div class="edit-photo">
                	<div class="current-photo"><img src="{{asset('img/choose-training-list-img.jpg')}}"></div>
                    <div class="change-photo"><label>Change Photo</label><input type="file" class="custom-file-input" name="">
                    <b>Please upload photo in squre size</b>
                    </div>
                    <div class="c"></div>
                </div>
                              
              <div>
              	{{ Former::submit('Save')->class('review-course-btn') }}
              	<div class="c"></div>
              </div>
              
          </div>
          {{ Former::close() }}
        </div>
		<div class="c"></div>
    </div>
</div>
@stop