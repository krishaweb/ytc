@extends('layouts.base')
@section('content')
<div class="content">
<div class="c"></div>
    <div class="content">
        <div class="inner-page">
        @if (Session::has('error'))
        {{ trans(Session::get('reason')) }}
        @endif
            <div class="login">
            {{
            Former::open()->rules(array('email' => 'required|email',
            'password' => 'required',
            'password_confirmation'=>'required'
            ))->method('post')
            }}
                <div class="login-form">
                <h3>Reset Password</h3>
                {{ Form::hidden('token',$token) }}
                    <div class="signup-email">
                    <div class="signup-email-icon"></div>
                    {{ Former::text('email')->placeholder('Mail')->label(false) }}
                    </div>

                    <div class="user-password">
                    <div class="password-icon"></div>
                    {{ Former::password('password')->placeholder('********')->label(false) }}
                    </div>

                    <div class="user-password-repeat">
                    <div class="user-password-repeat-icon"></div>
                    {{ Former::password('password_confirmation')->placeholder('********')->label(false) }}
                    </div>
                </div>

                <div class="login-bottom">
                {{ Former::actions()->success_submit('Submit') }}
                <div class="c"></div>
                </div>
                {{ Former::close() }}
            </div>
        </div>
    <div class="c"></div>
    </div>
</div>
@stop
