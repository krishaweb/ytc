<div class="video">
<div class="video-close"></div>
<div class="c"></div>
<iframe src="//www.youtube.com/embed/jO-EBdHu5Yw?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
<header>
  <div class="wrap">
      <div class="header-links">
        <a href="{{URL::to('for-providers')}}" class="for-providers">For Providers</a>
        @if(Auth::user() && !Auth::user()->admin)
          <div class="header-login-signup"><a href="{{ URL::to('my/courses') }}">My Account</a> / <a href="{{ URL::to('logout') }}">Logout</a></div>
        @else
          <div class="header-login-signup"><a href="{{URL::to('login')}}">Login</a> / <a href="{{URL::to('sign-up')}}">Sign Up</a></div>
        @endif
        </div>
        <div class="logo"><a href="{{URL::to('/')}}"><img src="{{asset('img/logo.png')}}" alt="logo"></a></div>
        
    <div class="nav">
          <ul>
              <li><a href="{{URL::to('/')}}" class="{{ Request::segment(1)==''?'active':'' }}">Home</a></li>
              <li><a href='{{URL::to("about-us")}}' class="{{ Request::segment(1)=='about-us'?'active':'' }}">About  Us</a></li>
              <li><a href="{{URL::to('choose-training')}}" class="{{ Request::segment(1)=='choose-training'?'active':'' }}">Choose Training</a></li>
              <li><a href="{{URL::to('resources')}}" class="{{ Request::segment(1)=='resources'?'active':'' }}">Resources</a></li>
                <li><a href='{{URL::to("for-providers")}}' class="{{ Request::segment(1)=='for-providers'?'active':'' }}">For Providers</a></li>
              <li><a href="{{URL::to('news')}}" class="{{ Request::segment(1)=='news'?'active':'' }}">Latest News</a></li>
              <li><a href='{{URL::to("contact-us")}}' class="{{ Request::segment(1)=='contact-us'?'active':'' }}">Get In Touch</a></li>
            </ul>
        </div>
  </div>
  
<div class="nav-m">
          <a href="#"><span>Menu <img src="{{asset('img/mobile-menu-icon.png')}}"></span>
          <div class="c"></div>
          </a>
            <div  class="nav-m1">
              <ul>
              <li><a href="{{URL::to('/')}}" class="{{ Request::segment(1)==''?'active':'' }}">Home</a></li>
              <li><a href='{{URL::to("about-us")}}' class="{{ Request::segment(1)=='about-us'?'active':'' }}">About  Us</a></li>
              <li><a href="{{URL::to('choose-training')}}" class="{{ Request::segment(1)=='choose-training'?'active':'' }}">Choose Training</a></li>
              <li><a href="{{URL::to('resources')}}" class="{{ Request::segment(1)=='resources'?'active':'' }}">Resources</a></li>
                <li><a href='{{URL::to("for-providers")}}' class="{{ Request::segment(1)=='for-providers'?'active':'' }}">For Providers</a></li>
              <li><a href='{{URL::to("news")}}' class="{{ Request::segment(1)=='news'?'active':'' }}">Latest News</a></li>
              <li><a href='{{URL::to("contact-us")}}' class="{{ Request::segment(1)=='contact-us'?'active':'' }}">Get In Touch</a></li>
            </ul>
            </div>
          </div>
  
</header>