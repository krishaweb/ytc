<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
{{ HTML::script('/js/jquery.js') }}
{{ HTML::script('/js/jquery.cycle2.js') }}
{{ HTML::script('/js/jquery.cycle2.carousel.js') }}
<script type="text/javascript">
$(function() {
      	//$('.index-banner ul li').css('background','none');
        $(".banner ul li > img,").each(function(i, elem) {
          var img = $(elem);
    		$(this).hide();
          $(this).parent().css({
            background: "url(" + img.attr("src") + ") no-repeat",
          });                  
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(".play-now-btn").click(function () {
	$('.video').show();
	});
    $(".video-close").click(function () {
	$('.video').hide();
});

$(document).keydown(function(e) {
    // ESCAPE key pressed
    if (e.keyCode == 27) {
        $('.video').hide();
    }
});

});
</script>

<script>
	$(document).ready(function () {
        $('.banner ul li').height($(window).height()-114);});
	$(window).resize(function() {
		$('.banner ul li').css('height', (window.innerHeight-114)+'px');
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(".accodian li h3").click(function () {		
		var current_li = $(this).parent();
		$(".accodian li div.refind-parameters").each(function(i,el) {			
			if($(el).parent().is(current_li)) {				
				$(this).parent().toggleClass("active");
				$(el).slideToggle();				
			} else{
				$(this).parent().removeClass("active");
				$(el).slideUp();
			}
		});
    });
	$('.accodian li > div.refind-parameters').hide();
		$('.accodian li').first().addClass("active");
		$('.accodian li > div.refind-parameters').first().show().addClass("active");
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
    $(".nav-m > a").click(function () {		
		var current_li = $(this).parent();
		$(".nav-m1").each(function(i,el) {			
			if($(el).parent().is(current_li)) {				
				//$(el).prev().toggleClass("plus");
				$(el).slideToggle();				
			} else{
				//$(el).prev().removeClass("plus");
				$(el).slideUp();
			}
		});
    });
	$('.nav-m .nav-m1 ').hide();
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
        $("#subscribe").submit(function(e){
            e.preventDefault();
            $('#inquiry_loading').show();
            var email = $("input#email").val();
            var dataString = 'email='+email; 
            $.ajax({
                type: "POST",
                url : $("#subscribe").prop('action'),
                data : dataString,
                success : function(data){
                	if(data=="ok")
                	{
                		$('#inquiry_loading').hide();
                		$('.subscribe-form').hide();
                		$('.subsuccess').text('Thank you for subscribe to Newsletters.');
                	}
                	else
                	{	
                		$('#inquiry_loading').hide();
                		$('.suberror').text(data);
                	}
                    
                }
            },"json");

    });
 });
</script>