{{Former::horizontal_open()->action(URL::route("listing"))->method('get')->id('search_form') }}
<div class="refind-your-choices">
            	<h2>Refine Your Choices</h2>
            <ul class="accodian">
               	  <li>
                   	  <h3><img src="{{asset('img/country-icon.png')}}">Country <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                        {{ Former::radios('country','')->radios(assoc_to_radios(array('australia'=>'australia','new zealand'=>'new zealand'),'country'))->check('australia') }}
                      </div>
                  </li>
                    
           	  <!-- <li>
                   	  <h3><img src="{{asset('img/location-icon.png')}}">Location <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="radio" value=""> Australia</label>
                      	<label><input name="" type="radio" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/profetion-icon.png')}}">Profession <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="radio" value=""> Australia</label>
                      	<label><input name="" type="radio" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/level-icon.png')}}">Level <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/accreditation-icon.png')}}">Accreditation <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/sector-icon.png')}}">Sector <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/delivery-method-icon.png')}}">Delivery Method <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/subject-icon.png')}}">Subject <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li> -->
                  
                  <li>
                  <div class="search-refine">
                  {{ Former::text('q','')->placeholder('Enter Any Keyword')->id('search_q') }}
              </div>
                  </li>
                  
            </ul>
              <div class="c"></div>

            </div>
              
              <div class="search-refine-submit">
              {{ Former::submit('Search Now!')->class('search-refine-submit-btn') }}
              </div>
              {{ Former::close() }}