@if(count($errors))
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  @foreach($errors->toArray() as $error)
    - {{implode(', ',$error)}}<br />
  @endforeach
  </div>
@endif