
@if(Session::has('success'))
<div class="flash-success">
  @if(Session::get('success') != 1)
    {{ Session::get('success') }}
  @else
    An e-mail with the password reset has been sent.
  @endif    
</div>
@endif




@if(Session::has('error'))
<div class="flash-error">
  @if(Session::get('error') != 1)
    {{ Session::get('error') }}
  @else
   {{ trans(Session::get('reason')) }}
  @endif
</div>
@endif
