<div class="footer">
    <div class="wrap">
        <ul>
            <li>
                <h4>Quick Link</h4>
                <a href="{{URL::to('/')}}">Home</a> / <a href="{{URL::to('about-us')}}">About Us</a> / <a href="{{URL::to('choose-training')}}">Choose Training</a> / <a href="{{URL::to('resources')}}">Resources</a> / <a href="{{URL::to('for-providers')}}">For Providers</a> / <a href="{{URl::to('news')}}">Latest News</a> / <a href="{{URL::to('contact-us')}}">Get In Touch</a>
            </li>
            
            <li>
                <h4>Get In Touch</h4>
                Liquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.
                <div class="social">
                <a href="#"><img src="{{asset('img/facebook.png')}}"></a>
                <a href="#"><img src="{{asset('img/twitter.png')}}"></a>
                <a href="#"><img src="{{asset('img/linkedin.png')}}"></a>
                </div>
            </li>
            
            <li>
                <h4>Contact Us</h4>
                Address : 85 Walpole Avenue<br>
WANGOOM VIC 3279<br>
Phone : (03) 5334 8967<br>
Email : <a href="mailto:admin@yourtrainingchoice.com.au">admin@yourtrainingchoice.com.au</a>
            </li>
        </ul>
        <div class="c"></div>
    </div>
</div>

<div class="copyright">
<div class="wrap">© 2014 Your Training Choice. Design and Developed by <a href="#">eTraffic Web Design</a></div>
</div>