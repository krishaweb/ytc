@section('title')
	{{ 'Get In Touch' }}
@stop
@section('content')
<div class="banner-inner">
  <h1>Get In Touch</h1>
</div>

<div class="get-in-touch">
	<div class="wrap">
    	<div class="get-in-touch-text1">
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. 
        </div>
        
<div class="get-in-touch-text2">
	If you have any questions or inquiries about our Course, please send us an email at<br>
    <a href="mailto:hello@yourtrainingchoice.com.au">hello@yourtrainingchoice.com.au</a> We´d love to hear from you!<br>
    Or fill in the form below and on of our specialists will help you.
</div>

{{Former::horizontal_open()->action(URL::to("contact-us"))->method('post') }}
{{ Former::hidden('ip','')->value(Request::getClientIp())}}
<div class="get-in-touch-form">
	<div>
		<div class="get-in-touch-form1">
			{{ Former::text('name','')->placeholder('Name') }}
		</div>
		<div class="get-in-touch-form2">
			{{ Former::text('phone','')->placeholder('Phone') }}
		</div>
		<div class="get-in-touch-form3">
			{{ Former::text('email','')->placeholder('Email') }}
		</div>
        <div class="c"></div>
    </div>
    
    <div class="get-in-touch-form-textarea">
    	{{ Former::textarea('message','')->placeholder('Message') }}
    </div>
    
    <div>
    	<div class="get-in-touch-captcha">
    		{{ HTML::image(Captcha::img(), 'Captcha image') }}
    		{{ Former::text('captcha','')->placeholder('Code Here') }}
          <div class="c"></div>
        </div>
    	<div class="get-in-touch-submit">
    		{{ Former::submit('Send Here')->class('get-in-touch-submit-btn') }}
    	</div>
        <div class="c"></div>
    </div>
</div>
{{ Former::close() }}
    </div>
</div>

<div class="get-in-touch-footer">
	<div class="wrap">
    	<ul>
        	<li>
            	<h4><img src="{{asset('img/terms-conditions-icon.png')}}">Terms & Conditions</h4>
                <div>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat <a href="{{URL::to('terms-and-conditions')}}">Read More</a></div>
            </li>
            
            <li>
            	<h4><img src="{{asset('img/news-updates-icon.png')}}">News & Updates</h4>
                <div>For news and updates please follow us on our blog, subscribe on <a href="#">Facebook</a> or follow us on <a href="#">Twitter</a>.</div>
            </li>
            
            <li>
            	<h4><img src="{{asset('img/contact-icon.png')}}">Contact</h4>
                <div>If you have any questions or inquiries about our Course, please send us an email at <a href="mailto:hello@yourtrainingchoice.com.au">hello@yourtrainingchoice.com.au</a> We´d love to hear from you!</div>
            </li>
            
            
        </ul>
        <div class="c"></div>
  </div>
</div>
@stop