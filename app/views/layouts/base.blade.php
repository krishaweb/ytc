<!doctype html>
<html>
<head>
<meta charset="UTF-8">
@include('shared.meta')
@yield('meta')    

<title>
  {{ Config::get('site.name') }} | @yield('title')   
</title>
  
<!-- styles -->
{{ HTML::style('/css/style.css') }}
{{ HTML::style('/css/media.css') }}

@yield('styles')
<!-- styles -->    

</head>
<body>
  @include('shared.header')    
  @yield('content')
  @include('shared.footer')
  
  <!-- scripts -->
  @include('admin.shared.vars')
  @yield('vars')  
  @include('shared.script')
  @yield('scripts')
  
  <!-- scripts -->
  </body>
</html>