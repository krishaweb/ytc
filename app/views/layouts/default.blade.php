<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	@include('shared.meta')
	@yield('meta')		

	<title>
		{{ Config::get('site.name') }} | @yield('title')	 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
	<!-- styles -->
	{{ HTML::style('/css/style.css') }}
	{{ HTML::style('/css/media.css') }}
	<!-- {{ HTML::style('/css/ui-lightness/jquery-ui.min.css')}} -->
	{{ HTML::style('/css/demo.css') }}
	{{ HTML::style('/css/calendar.css') }}
	{{ HTML::style('/fancybox2/jquery.fancybox.css') }}
	{{ HTML::style('/css/jquery-ui-timepicker-addon.css') }}
	@if(Request::segment(1)=='whats-on')
	{{ HTML::style('/css/custom_1.css') }}
	@else
	{{ HTML::style('/css/custom_2.css') }}
	@endif
	@yield('styles')
	<!-- styles -->    
</head>
	@if(Request::segment(1)=='whats-on')
	<body class="whats_on"> 
	@else
	<body>
	@endif
		@include('shared.header1')		
		@yield('content')
		@include('shared.footer')
	
	<!-- scripts -->
	@include('admin.shared.vars')
	@yield('vars')	
	
	{{ HTML::script('//code.jquery.com/jquery-1.11.0.min.js') }}
	
	{{ HTML::script('/js/html5.js') }}
	{{ HTML::script('/js/angular.min.js') }}
	{{ HTML::script('/js/jquery.validate.js') }}
	{{ HTML::script('/js/process.js') }}
	{{ HTML::script('/js/modernizr.custom.63321.js') }}
	{{ HTML::script('/js/jquery.hoverIntent.minified.js') }}
	{{ HTML::script('/js/jquery.cycle2.js') }}
	{{ HTML::script('/js/jquery.cycle2.carousel.js') }}
	{{ HTML::script('/js/jquery.calendario.js') }}
	{{ HTML::script('/js/data.js') }}
	{{ HTML::script('/fancybox2/jquery.fancybox.js') }}
	{{ HTML::script('/js/jquery-ui.min.js') }}
	{{ HTML::script('/js/all.js') }}	
	{{ HTML::script('/js/jquery-ui-timepicker-addon.js') }}
	{{ HTML::script('/js/jquery-ui-sliderAccess.js') }}
	{{ HTML::script('/js/jquery.geocomplete.min.js') }}
	{{ HTML::script('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places') }}	
	{{ HTML::script('https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js') }}
	<!-- {{ HTML::script('/js/placeholder.js') }} -->

	@yield('scripts')
	@yield('whatson_scripts')
	
	<!-- scripts -->
	</body>
</html>