@section('title')
	Simplify Your Choices Here
@stop

@section('content')
<div class="banner-inner">
  <h1>Simplify Your Choices Here.</h1>
</div>

<div class="choose-training">
	<div class="wrap">
		<div class="choose-training-left">
        	<div class="set-your-parameters">
            	<div class="set-your-parameters-text">Set your parameters below and 'Search' to find Your Training Choice.</div>
    	    </div>
            
            
<div class="choose-training-steps" id="choose-training-steps">
	<ul>
		<li>
			<h3>Step 1</h3>
			<div class="choose-training-steps-disc">Select Your Parameters For Your Course And Recive A Complete List of Options That Meet Your Needs</div>
		</li>

		<li>
			<h3>Step 2</h3>
			<div class="choose-training-steps-disc">Follow The “View Course” Button To Contact That Provider Directly And Get In Touch With Your Training Choice.</div>
		</li>
</ul>
</div>
  @include('shared.search')
            
	    </div>
        
		<div class="choose-training-right">
        	<div class="choose-training-steps">
            	<ul>
                	<li>
                    	<h3>Step 1</h3>
                        <div class="choose-training-steps-disc">Select Your Parameters For Your Course And Recive A Complete List of Options That Meet Your Needs</div>
                    </li>
                    
                	<li>
                    	<h3>Step 2</h3>
                        <div class="choose-training-steps-disc">Follow The “View Course” Button To Contact That Provider Directly And Get In Touch With Your Training Choice.</div>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div class="c"></div>
        
    
    </div>
</div>

@stop