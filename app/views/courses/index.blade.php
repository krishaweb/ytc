@section('title')
	Simplify Your Choice Here
@stop

@section('content')
<div class="banner-inner">
  <h1>Simplify Your Choices Here.</h1>
</div>

<div class="choose-training">
	<div class="wrap">
		<div class="choose-training-left">
        	<div class="set-your-parameters">
            	<div class="set-your-parameters-text">Set your parameters below and 'Search' to find Your Training Choice.</div>
    	    </div>            
          <div class="refind-your-choices">
            	<h2>Refine Your Choices</h2>
              <form action="" method="get">
            <ul class="accodian">
               	  <li>
                   	  <h3><img src="{{asset('img/country-icon.png')}}">Country <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> New Zealand</label>
                      </div>
                  </li>
                    
           	  <li>
                   	  <h3><img src="{{asset('img/location-icon.png')}}">Location <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="radio" value=""> Australia</label>
                      	<label><input name="" type="radio" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/profetion-icon.png')}}">Profession <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="radio" value=""> Australia</label>
                      	<label><input name="" type="radio" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/level-icon.png')}}">Level <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/accreditation-icon.png')}}">Accreditation <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/sector-icon.png')}}">Sector <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/delivery-method-icon.png')}}">Delivery Method <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                    
               	  <li>
                   	  <h3><img src="{{asset('img/subject-icon.png')}}">Subject <div class="accodian-arrow"></div></h3>
                      <div class="refind-parameters">
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      	<label><input name="" type="checkbox" value=""> Australia</label>
                      </div>
                  </li>
                  
                  <li>
                  <div class="search-refine">
              	<input name="q" type="text" placeholder="Enter Any Keyword" value="{{Input::get('q')}}">
              </div>

                  </li>
                  
            </ul>
            </form>
              <div class="c"></div>

            </div>
              
              <div class="search-refine-submit">
              	<input type="submit" class="search-refine-submit-btn" value="Search Now!!">
              </div>
              

            
	    </div>
        
		<div class="choose-training-right choose-training-list-part">
        	<div class="choose-training-list">
            	<div class="choose-training-list-disc">
                	<h2>{{$result->getTotal()}} courses:</h2>
                    <div class="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </div>
                    <div class="download-these-results-btn"><a href="#">Download These Results</a></div>
                </div>
            </div>
            
            <div class="choose-training-listing">
            	<ul>
              @foreach($result->getResults() as $r)
                	<li>
                 
                   	<div class="choose-training-listing-left" id="course-{{$r->id}}">
	                    	<h3><a href="#">{{$r->title}}</a></h3>
                          <div class="choose-training-listing-disc">{{$r->description}}</div>
                            <div class="choose-training-listing-left-icon">
                       	    <img src="{{asset('img/country-icon.png')}}">
                            <img src="{{asset('img/location-icon.png')}}">
                            <img src="{{asset('img/profetion-icon.png')}}">
                            <img src="{{asset('img/level-icon.png')}}">
                            <img src="{{asset('img/accreditation-icon.png')}}">
                            <img src="{{asset('img/sector-icon.png')}}">
                            <img src="{{asset('img/delivery-method-icon.png')}}">
                            <img src="{{asset('img/subject-icon.png')}}">
                            <img src="{{asset('img/course-date.png')}}">
                            <img src="{{asset('img/price-icon.png')}}"> </div>
                      </div>
                        
                    	<div class="choose-training-listing-right">
                        	<div class="choose-training-listing-img"><img src="images/choose-training-list-img.jpg"></div>
                            <div class="choose-training-listing-price">$10,000</div>
                            <div class="view-course-btn"><a href="view-course-btn">View Course</a></div>
                      </div>
                    	<div class="c"></div>
                    </li>
              @endforeach
                </ul>
            </div>
            
			<div class="pagination">
            	<ul>
                	<li><a href="#"></a></li>
                	<li><a href="#">1</a></li>
                	<li class="active"><a href="#">2</a></li>
                	<li><a href="#">3</a></li>
                	<li><a href="#">4</a></li>
                	<li><a href="#">5</a></li>
                	<li><a href="#"></a></li>
                </ul>
            </div>
            
            
        </div>
        <div class="c"></div>
        
    
    </div>
</div>

@stop