@section('title')
	{{'Our News'}}
@stop

@section('content')
<div class="banner-inner">
    <h1>Our News</h1>
</div>

<div class="news">
    <div class="wrap">
        <div class="news-left">
            <div class="news-list">
            <ul>
            @foreach($posts as $p)
                <li>
                    <div class="news-img"><img src="{{ $p->photo_url('medium') }}"></div>
                  <h3><a href='{{ URL::to("news/{$p->id}-{$p->slug}") }}'>{{$p->title}}</a></h3>
                    <div class="news-disc">{{ $p->intro }}</div>
                    <div class="news-info"><img src="{{asset('img/latest-news-by.jpg')}}"> by Chris Meier • <a href="#">Design</a> • <a href="#">1 Comment</a> • {{ $p->date->format('j, M Y') }}
                    <div class="news-readmore"><a href='{{ URL::to("news/{$p->id}-{$p->slug}") }}'>Read More</a></div>
                  </div>
                </li>
            @endforeach
            @if(count($posts) == 0)
                    <li>
                        <h3>No news found. </h3>
                    </li>
                @endif
            </ul>
            </div>
            <div class="pagination">
                {{ $posts->links() }}
            </div>
        </div>
        <div class="news-right">
            <div class="subscribe">
                <h2>Subscribe</h2>
                <div>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim.</div>
                <div id="inquiry_loading" style="display:none;"><img src="{{asset('img/loading.gif')}}" alt="Your Training Choice"></div>
                <div class="suberror"></div>
                <div class="subsuccess"></div>
                <div class="subscribe-form">
                    {{ Form::open(array('action'=>'SubscribesController@postIndex',"id"=>"subscribe")) }}
                    {{ Form::text('email',Input::get('email'),array('placeholder'=>'e-mail address','id'=>'email')) }}
                    {{ Form::submit('Subscribe',array('class'=>'subscribe-btn')) }}
                    {{ Form::close() }}
                </div>
            </div>
            
            <div class="recent-news">
                <h2>Recent Post</h2>
                <ul>
                @foreach($recent_post as $rcn)
                    <li><a href='{{URL::to("news/{$rcn->id}-{$rcn->slug}")}}'>
                        <div class="news-th"><img src="{{$rcn->photo_url('thumb')}}"></div>
                        <div class="recent-news-disc">
                            <h5>{{$rcn->title}}</h5>
                            <div class="recent-news-info">Post by : Admin    /    {{ $p->date->format('j. M. Y') }}</div>
                        </div>
                    <div class="c"></div>                        
                    </a>
                    </li>
                @endforeach 
                @if(count($posts) == 0)
                <div class="recent-news-disc">
                    <li>
                        <h5>No news found. </h5>
                    </li>
                </div>
                @endif   
                    
                </ul>
                <div class="c"></div>
            </div>
            
        </div>
        <div class="c"></div>
    </div>
</div>

@stop