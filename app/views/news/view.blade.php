@section('title')
	{{'Our News'}}
@stop

@section('content')
<div class="banner-inner">
    <h1>Our News</h1>
</div>

<div class="news">
    <div class="wrap">
        <div class="news-left">
            
            <div class="news-detail">
              <h2>{{$post->title}}</h2>
                <div class="news-detail-img"><img src="{{$post->photo_url('medium')}}"></div>
                <div class="news-info"><img src="{{asset('img/latest-news-by.jpg')}}"> by Chris Meier • <a href="#">Design</a> • <a href="#">1 Comment</a> • {{$post->date->format('j, M Y')}}</div>
                
                <div class="news-disc">
                    {{$post->content}}
                </div>
            </div>    
        </div>
        <div class="news-right">
            <div class="subscribe">
                <h2>Subscribe</h2>
                <div>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nulla consequat massa quis enim.</div>
                <div id="inquiry_loading" style="display:none;"><img src="{{asset('img/loading.gif')}}" alt="Your Training Choice"></div>
                <div class="suberror"></div>
                <div class="subsuccess"></div>
                <div class="subscribe-form">
                    {{ Form::open(array('action'=>'SubscribesController@postIndex',"id"=>"subscribe")) }}
                    {{ Form::text('email',Input::get('email'),array('placeholder'=>'e-mail address','id'=>'email')) }}
                    {{ Form::submit('Subscribe',array('class'=>'subscribe-btn')) }}
                    {{ Form::close() }}
                </div>
            </div>
            
            <div class="recent-news">
                <h2>Recent Post</h2>
                <ul>
                @foreach($recent_post as $rcn)
                    <li><a href="#">
                        <div class="news-th"><img src="{{$rcn->photo_url('thumb')}}"></div>
                        <div class="recent-news-disc">
                            <h5>{{$rcn->title}}</h5>
                            <div class="recent-news-info">Post by : Admin    /    {{$rcn->date->format('j. M. Y')}}</div>
                        </div>
                    <div class="c"></div>                        
                    </a>
                    </li>
                @endforeach 
                @if(count($recent_post) == 0)
                    <div class="recent-news-disc">
                        <li>
                            <h5>No recent post found.</h5>
                        </li>
                    </div>
                @endif   
                </ul>
                <div class="c"></div>
            </div>
            
        </div>
        <div class="c"></div>
    </div>
</div>

<div class="comment">
    <div class="wrap"><img src="{{asset('img/comment.jpg')}}"></div>
</div>


@stop

@section('scripts')

@stop