<?php

class CoursesController extends BaseController {

	public $restful = true;
	public $timestamps = true;

	public function getIndex($slug = null) {

		// $query = [ 'filtered' => [
  //   		'query' => [ 'match' => [ 'title' => Input::get('q') ]],
  //   		'filter' => [ 'term' => [ 'country' => Input::get('country') ]],
		// ]];

		// $hits = Course::search($query);

		// dd($hits);
	
		  // $query['query']['match']['title'] = Input::get('q');
		  // $results = Course::search(null, ['query' => $query])->getResults();
		$query = [
			
		];

		$q = Input::get('q');
		if($q) {
			$query["query"] = [ "bool" => [
					"should" => [
						[ "match" => [ 'title' => Input::get('q', '*') ]],
					    [ "match" => [ 'description' => Input::get('q', '*') ]]
					]
				]
			];
		} else {
			$q = '*';
		}
		$result = Course::search($q, compact('query'));

		#return Response::json($results);

		$this->layout->content = View::make('courses.index',compact('result'));		
	}

	public function getChoose() {
		$this->layout->content = View::make('courses.choose');		
	}


	public function getView($id,$slug = null) {

		$this->layout->content = View::make('courses.view');

	}

}
