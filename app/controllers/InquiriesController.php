<?php

class InquiriesController extends BaseController {
	public $restful = true;

	public function getIndex()
	{		
		$this->layout->content = View::make("inquiries.index");		
	}

	public function postIndex() {

        $rules = array(
            'name' => 'required',
            'phone' => 'numeric',
            'email' => 'required|email',
            'captcha' => 'required|captcha'
        );
        $messages = array(
            'name.required' => 'Please enter your name',
            'phone.numeric' => 'Please enter only digits',
            'email.required' => 'Please enter your email address',
            'email' => 'Please enter valid email address',
            'captcha.required' => 'Please enter security code',
            'captcha' => 'Please enter correct security code'
        );
        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->passes()) {
            try {
                $inquiry = new Inquiry(Input::all());
                $inquiry->ua = $_SERVER['HTTP_USER_AGENT'];
                $inquiry->created_at = date('Y-m-d H:i:s');
                $inquiry->save();
                $data = Input::all();
                Mail::send('emails.contact', array(), function($message) {
                            $message->from(Config::get('site.email'), Config::get('site.name'));
                            $message->to('niravp@krishaweb.com');
                            $message->subject('Inquiry from ' . Config::get('site.name'));
                        });

                return Redirect::to('thankyou');
            } catch (Exception $e) {
                return Redirect::back()
                                ->withErrors($validator)
                                ->withInput()
                                ->with('error', $e->getMessage());
            }
        }

        return Redirect::back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('error','Please correct the following errors');
    }
	

}