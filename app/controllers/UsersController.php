<?php

/**
* 
*/
class UsersController extends BaseController
{
	
	public $restful=true;



 	public function getLogin() {

		if (!Auth::guest()) {
			return Redirect::to('/')->with("warning", 'You are already logged in.');
		}

		if (Cookie::get('auth_remember')) {
			$user_id = Crypter::decrypt(Cookie::get('auth_remember'));
			Auth::login($user_id);
			
			return Redirect::to('/')->with('success', 'You have logged in successfully.');
		}
		$this->layout->content = View::make('users.login');
	}

	 public function postLogin() {
	 	$rules=array('email'=>'required','password'=>'required');
		$validator=Validator::make(Input::all(),$rules);
		if($validator->passes())
		 	{
		$field=filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL) ?'email':'username';
		$credentials = array($field => Input::get('email'), 'password' => Input::get('password'),'status'=>'1');


		if (Auth::attempt($credentials)	) {

			$user = User::find(Auth::user()->id);
			$user->last_login = date('Y-m-d H:i:s');
			$user->ip = getenv("REMOTE_ADDR");
			$user->save();
			
			return Redirect::to('/')->with('success', 'You have logged in successfully.');
		} 
		else {
			return Redirect::back()->with('error', 'Invalid username or password')->withInput(Input::except('password'));
		}
		}
		return Redirect::back()
		->withErrors($validator)
		->withInput()
		->with('error', 'Please correct following errors');
	}


 	public function getRegister() 
 	{

		$row = new User(Input::old());
		Former::populate($row);
		$this->layout->content = View::make('users.register',compact('row'));


	}
	public function postRegister()
	{
		try {
		$rules = array(
			'fname'=>'required',
			'lname'=>'required',
			'email'=>'required|email|unique:users',
			'password'=>'required|alpha_num|min:6|max:20|confirmed',
			'password_confirmation'=>'required'
			);

		$messages = array(
			'fname.required' => 'Please enter your first name',
			'lname.required' => 'Please enter your last name',
			'email.required' => 'Please enter you email address',
			'email' => 'Please enter valid email address',
			'password.required' => 'Please enter your password',
			'password_confirmation.required' => 'Please enter repeat password'
			);

	$validator = Validator::make(Input::all(), $rules, $messages);
	
	if ($validator->passes()) {

		
			$reset_token = md5(time());
			$user = new User(Input::all());
			$user->name = Input::get('fname') . ' ' . Input::get('lname');
			$user->role = 'provider';
			$user->username = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->reset_token = $reset_token;
			$user->ip=getenv("REMOTE_ADDR");
			$user->save();

			$email = $user->email;
			$data = array( 'email' => $email, 'from' => 'info@yourtrainingchoice.com.au','reset_token' => $reset_token);
			$send =	Mail::send('emails.activation', $data, function($message) use($data)
				{
					$message->from($data['from'],'Your Training Choice');
					$message->to($data['email']);
					$message->subject('Activation link to login in Your Training Choice.');
				});
			return Redirect::to('login')->with('success'," Confirm your email address to access all of Your Training Choice's features. A confirmation message was sent to #{$email}.");

		} 
	}
		catch (Exception $e) 
		{
			return Redirect::back()
			->withErrors($validator)
			->withInput()
			->with('error', $e->getMessage());
		}
	


	return Redirect::back()
	->withErrors($validator)
	->withInput()
	->with('error', 'Please correct following errors');
	}

 	public function getActive($reset_token){


		if($reset_token){
			$count = User::where('reset_token','=',$reset_token)->count();
		}

		if(!$count) {
			return Redirect::to('/')->with('error', 'No user found.');
		}
		else{
			$row = User::where('reset_token','=',$reset_token)->get();
			foreach($row as $r):
			$id = $r->id;
			endforeach;


			$user = User::find($id);
			$user->reset_token = '';
			$user->status = '1';
			$user->save();
			return Redirect::to('login')->with('success','Your account is approved.');

		}
	}
 	public function getForgot(){

		$row = new User(Input::old());
		Former::populate($row);
		$this->layout->content = View::make('users.forgot',compact('row'));
    }

     public function postForgot(){

		$rules = array(
		'email' => 'required|email'
		);
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {
			$credentials = array('email' => Input::get('email'));
			Password::remind($credentials, function($mail) {
			$mail->subject("Platinum-luxury : Reset password instructions");
			});

			return Redirect::To("login")->with('success',"Mail has been sent on your email Id.");
		}
		return Redirect::back()
		->withErrors($validator)
		->withInput()
		->with('error', 'Please correct following errors');
	}


	public function getReset($token) {
		$row = new User(Input::old());
		Former::populate($row);
		$this->layout->content = View::make("users.reset", compact('token'));
	}

	 public function postReset($token) {

		$rules = array(
		'email' => 'required|email',
		'password' => array('required', 'min:6', 'max:20', 'alpha_num', 'confirmed'),
		'password_confirmation' => array('required', 'alpha_num')
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {

			
				$credentials = array('email' => Input::get('email'),'password'=>Input::get('password'),'password_confirmation'=>Input::get('password_confirmation'),'token'=>Input::get('token'));
				
				Password::reset($credentials, function($user, $password)
				{
				$user->password = Hash::make($password);
				$user->save();
				});
		return Redirect::to('login')->with('success',"Password successfully changed");

		}


		return Redirect::back()
		->withErrors($validator)
		->withInput()
		->with('error', 'Please correct following errors');

	}
 	public function getLogout() {
		Auth::logout();
		return Redirect::to('login')->with('success', 'You have logged out successfully.');
	}

	public function getProfile() {
		if (Session::has('errors')) {
      		$row = new User(Input::old());      
    	} else {
      		$row = User::find(Auth::user()->id);
    	}    
	    Former::populate($row);
	    $this->layout->content = View::make("users.profile", compact('row'));
	}

	public function postProfile() {
		$rules = array(
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'url' => 'url'
        );
        $messages = array(
            'name.required' => 'Please enter provider name',
            'phone.required' => 'Please enter contact number',
            'email.required' => 'Please enter provider email address',
            'email' => 'Please enter valid email address',
            'url' => 'Please enter valid url'

        );

		$validator = Validator::make(Input::all(), $rules, $messages);

	    if ($validator->passes()) {
	      try {
	        $provider = User::find(Auth::user()->id);
	        $provider->fill(Input::all());
	        $provider->save();
	        return Redirect::to('my/edit-profile')->with('success', 'Profile updated successfully');
	      } catch (Exception $e) {
	        return Redirect::back()
	                        ->withErrors($validator)
	                        ->withInput()
	                        ->with('error', $e->getMessage());
	      }
	    }

	    return Redirect::back()
	                    ->withErrors($validator)
	                    ->withInput()
	                    ->with('error', 'Please correct following errors');
	}

	public function getChangePassword(){
		$this->layout->content = View::make("users.change_password");
	}

	public function postChangePassword(){
		$rules = array(
	        'old_password'              => 'required',
	        'password'                  => 'required|confirmed|different:old_password',
	        'password_confirmation'     => 'required|different:old_password'
    	);

    	$messages = array(
    		'old_password.required' => 'Please enter your old password',
    		'password.required' => 'Please enter your new password',
    		'password_confirmation.required' => 'Please enter confirm new password'
    	);

	    $validator = Validator::make(Input::all(), $rules, $messages);
	    if($validator->passes()){
	        $user = User::findOrFail(Auth::user()->id);
	        if(!Hash::check(Input::get('old_password'), $user->password)){
        		return Redirect::back()->withInput()->with('error','Please enter correct old passsword');
    		}
	        $user->password = Hash::make(Input::get('password'));
	        $user->save();
	        return Redirect::back()->with('success', 'Your password is changed successfully');
	    }else {
	        return Redirect::back()
	                    ->withErrors($validator)
	                    ->withInput()
	                    ->with('error', 'Please correct following errors');
	    }
	}

	public function getCourse(){
		$query = Course::where('provider_id','=',Auth::user()->id);
		$courses = $query->paginate(10);
		$this->layout->content = View::make('users.course',compact('courses'));
	}
}


