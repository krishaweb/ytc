<?php

class ResourcesController extends BaseController {

	public $restful = true;
	public $timestamps = true;
	public function getIndex($slug = null) {

		$query = Resource::query()->where('status', '=', true);

		$resources = $query->paginate(12);

		$this->layout->content = View::make('resources.index',compact('resources'));		
	}

	public function getDownload($file){
        $file= public_path(). "/a/uploads/resources/$file";
        $filename = $file. ".pdf";
        $headers = array(
              'Content-Type: application/pdf',
            );
        return Response::download($file, 'filename.pdf', $headers);
}
	
}