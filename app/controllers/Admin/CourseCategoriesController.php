<?php

class Admin_CourseCategoriesController extends Admin_BaseController {

  public $restful = true;

  public function getIndex() { 
        
    if(Input::get('parent_id')) {
      $parent = Course_Category::find(Input::get('parent_id'));
      $query = $parent->descendantsAndSelf();
    } else {
      $query = Course_Category::query();
    }
    
    $query->orderBy('course_categories.lft','asc');
    $query->where(function($q) {
      if(Input::get('id')) $q->where('id','=',Input::get('id'));      
      if(Input::get('name')) $q->where('name','LIKE','%'.Input::get('name').'%');
    });
    
    $rows = $query->paginate(Config::get('site.paging.limit'));
    $parents = Course_Category::lists('name', 'id');
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.list", compact(
                            'rows', 'parents'
    ));
  }

  public function getAdd() {   
    $edit = false;
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.form", compact(
                            'edit'
    ));
  }

  public function postAdd() {
    $validator = Validator::make(Input::all(), Course_Category::$rules);
    if ($validator->passes()) {
      $row = new Course_Category(Input::all());
      $row->save();
      
      if(Input::get('parent_id')) {
        $parent = Course_Category::find(Input::get('parent_id'));
        $row->makeChildOf($parent);
      }

      $url = Input::get('next', "{$this->prefix}/{$this->controller}");
      return Redirect::to($url)->with('success', 'Saved successfully');
    }
    Former::withErrors($validator);
    return $this->getAdd();
  }

  public function getEdit($id) {    
    $edit = true;
    $row = Course_Category::find($id);   
    
    Former::populate( $row );
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.form", compact(
                            'edit','row'
    ));
  }
  
  public function postEdit($id) {
    $validator = Validator::make(Input::all(), Region::$rules);
    if ($validator->passes()) {
      $row = Course_Category::find($id);
      $row->fill(Input::all());
      $row->save();
           
      if(Input::get('parent_id')) {
        $parent = Course_Category::find(Input::get('parent_id'));
        $row->makeChildOf($parent);
      }

      $url = Input::get('next', "{$this->prefix}/{$this->controller}");
      return Redirect::to($url)->with('success', 'Saved successfully');
    }
    Former::withErrors($validator);
    return $this->getEdit($id);
  }
  
  public function getDelete($id) {
    $url = Input::get('next', "{$this->prefix}/{$this->controller}");
    try {      
      $row = Course_Category::find($id);
      $row->delete();      
      return Course_Category::to($url)->with('success', 'Deleted successfully');
    } catch(Exception $e) {
      return Redirect::to($url)->with('error', $e->getMessage());
    }
  }

}
