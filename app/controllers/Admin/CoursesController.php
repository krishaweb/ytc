<?php

class Admin_CoursesController extends Admin_BaseController {

  public function index() {

    $query = Course::with(array('category','provider'));
    $query->orderBy('updated_at', 'desc');
    $query->where(function($q) {
              if (Input::get('id'))
                $q->where('id', '=', Input::get('id'));
              if (Input::get('title'))
                $q->where('title', 'LIKE', '%' . Input::get('title') . '%');
              if(Input::get('category_id')) $q->where('category_id','=',Input::get('category_id'));
              if(Input::get('provider_id')) $q->where('provider_id','=',Input::get('provider_id'));            
              if(Input::get('status','')!=='') $q->where('status','=',Input::get('status'));      
            });
    $rows = $query->paginate();    
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.list", compact(
                            'rows'
    ));
  }

  public function create() {
    $row = new Course(Input::old());
    $edit = false;

    Former::populate($row);
    $this->layout->content = View::make("admin.courses.form", compact(
                            'edit', 'row'
    ));
  }

  public function store() {
    $validator = Validator::make(Input::all(), Course::$rules);

    if ($validator->passes()) {
      try {
        $post = new Course(Input::all());
        $post->save();

        $url = Input::get('next', "{$this->prefix}/{$this->controller}");
        return Redirect::to($url)->with('success', 'Saved successfully');
      } catch (Exception $e) {
        return Redirect::back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('error', $e->getMessage());
      }
    }

    return Redirect::back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('error', 'Please correct following errors');
  }

  public function edit($id) {
    //$row = Profile::find($id);
    $edit = true;
    if (Session::has('errors')) {
      $row = new Course(Input::old());      
    } else {
      $row = Course::with(array('category','provider'))->find($id);
    }    
    Former::populate($row);
    $this->layout->content = View::make("admin.courses.form", compact(
                            'edit', 'row', 'id'
    ));
  }

  public function update($id) {
    $validator = Validator::make(Input::all(), Course::$rules);

    if ($validator->passes()) {
      try {
        $post = Course::find($id);
        $post->fill(Input::all());
        $post->save();

        $url = Input::get('next', "{$this->prefix}/{$this->controller}");
        return Redirect::to($url)->with('success', 'Saved successfully');
      } catch (Exception $e) {
        return Redirect::back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('error', $e->getMessage());
      }
    }

    return Redirect::back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('error', 'Please correct following errors');
  }

  public function destroy($id) {
    $url = Input::get('next', "{$this->prefix}/{$this->controller}");
    $row = Course::find($id);
      $row->delete();
      return Redirect::to($url)->with('success', 'Deleted successfully');
    
  }

}