<?php

class Admin_ResourcesController extends Admin_BaseController {

  public function index() {

    $query = Resource::query();
    $query->orderBy('updated_at', 'desc');
    $query->where(function($q) {
              if (Input::get('id'))
                $q->where('id', '=', Input::get('id'));
              if (Input::get('title'))
                $q->where('title', 'LIKE', '%' . Input::get('title') . '%');
              if(Input::get('category_id')) $q->where('category_id','=',Input::get('category_id'));      
              if(Input::get('status','')!=='') $q->where('status','=',Input::get('status'));      
            });
    $rows = $query->paginate();    
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.list", compact(
                            'rows'
    ));
  }

  public function create() {
    $row = new Resource(Input::old());
    $edit = false;
    Former::populate($row);
    $this->layout->content = View::make("admin.resources.form", compact(
                            'edit', 'row'
    ));
  }

  public function store() {
    $validator = Validator::make(Input::all(), Resource::$rules);

    if ($validator->passes()) {
      try {
        $post = new Resource(Input::all());
        $post->save();

        $url = Input::get('next', "{$this->prefix}/{$this->controller}");
        return Redirect::to($url)->with('success', 'Saved successfully');
      } catch (Exception $e) {
        return Redirect::back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('error', $e->getMessage());
      }
    }

    return Redirect::back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('error', 'Please correct following errors');
  }

  public function edit($id) {
    //$row = Profile::find($id);
    $edit = true;
    if (Session::has('errors')) {
      $row = new Resource(Input::old());      
    } else {
      $row = Resource::find($id);
    }    
    Former::populate($row);
    $this->layout->content = View::make("admin.resources.form", compact(
                            'edit', 'row', 'id'
    ));
  }

  public function update($id) {
    $validator = Validator::make(Input::all(), Resource::$rules);

    if ($validator->passes()) {
      try {
        $post = Resource::find($id);
        $post->fill(Input::all());
        $post->save();

        $url = Input::get('next', "{$this->prefix}/{$this->controller}");
        return Redirect::to($url)->with('success', 'Saved successfully');
      } catch (Exception $e) {
        return Redirect::back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('error', $e->getMessage());
      }
    }

    return Redirect::back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('error', 'Please correct following errors');
  }

  public function destroy($id) {
    $url = Input::get('next', "{$this->prefix}/{$this->controller}");
    $row = Resource::find($id);
      $row->delete();
      return Redirect::to($url)->with('success', 'Deleted successfully');
    
  }

}