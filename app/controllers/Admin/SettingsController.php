<?php

class Admin_SettingsController extends Admin_BaseController {

  public $restful = true;

  public function getIndex() {
    //$this->layout->nest('content', 'admin.settings.form');
    $this->layout->content = View::make('admin.settings.form');
  }

  public function postIndex() {
    $rules = array(
        'site_email' => 'required|email',
        'site_phone' => 'required',
        'site_facebook_url' => 'url',
        'site_twitter_url' => 'url',
        'site_googleplus_url' => 'url',
    );
        
    $validation = Validator::make(Input::all(), $rules);
    if ($validation->fails()) {
      return Redirect::back()
                      ->withErrors($validation)
                      ->withInput()
                      ->with("error", "Please correct following details.");
    }

    foreach (Input::all() as $id => $value) {
      $id = str_replace('_', '.', $id);
      $setting = Setting::find($id);
      if (!$setting) {
        $setting = new Setting();
        $setting->id = $id;
      }
      $setting->value = $value;
      $setting->save();
    }

     Cache::forget('settings');
     return Redirect::back()->with('success', 'Saved successfully.');
  }

}