<?php

class Admin_ContentsController extends Admin_BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$query = Content::query();
	    if(Input::get('id')) $query->where('id','=', Input::get('id'));
	    if(Input::get('name')) $query->where('name','LIKE', Input::get('name'));
	    $row = new Content(Input::old());
	    $rows = $query->paginate();    
	    $this->layout->content = View::make('admin.contents.list', compact('rows','row'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$row = new Content(Input::old());
		$id = false;
		Former::populate($row);
		$this->layout->content = View::make("{$this->prefix}.{$this->controller}.form", compact('id','row'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator= Validator::make(Input::all(),Content::$rules);

		if($validator->passes())
		{	
			try 
			{
				$content=new Content(Input::all());
				$content->save();
				$url = Input::get('next', "{$this->prefix}/{$this->controller}");
				return Redirect::to($url)->with('success', 'Saved successfully');	
			} 
			catch (Exception $e) 
			{
				return Redirect::back()
			    ->withErrors($validator)
			    ->withInput()
			    ->with('error', $e->getMessage());				
			}
			
		}
	return Redirect::back()
	->withErrors($validator)
	->withInput()
	->with("error","Please correct following details.");
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		Former::populate( Content::find($id) );
		if (Session::has('errors'))
		{
			$row = new Content(Input::old());
		} 
		else
		{
			$row = Content::find($id);
		}
		Former::populate($row);
    	$this->layout->content = View::make('admin.contents.form', compact('id','row'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		Content::$rules['code'].=",$id";
		$validator = Validator::make(Input::all(),Content::$rules );
    if($validator->passes()) 
    {
    	try 
    	{
	      $car = Content::find($id);
	      $car->fill( Input::all() );
	      $car->save();
	      $url = Input::get('next', "{$this->prefix}/{$this->controller}");
		  return Redirect::to($url)->with('success', 'Saved successfully');     		
    	} 
    	catch (Exception $e) 
    	{
    		return Redirect::back()
			->withErrors($validator)
			->withInput()
			->with('error', $e->getMessage());
    	}     
    }

    return Redirect::back()
	->withErrors($validator)
	->withInput()
	->with("error","Please correct following details.");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$content = Content::find($id);
	    $content->delete();
	    if(Request::ajax())
	      return Response::json('OK');
	    else
	      return Redirect::route('admin.contents.index');
	}


}
