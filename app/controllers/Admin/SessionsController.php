<?php

class Admin_SessionsController extends Admin_BaseController {


public $layout = 'admin.layouts.login';
public $restful = true;


public function getLogin() {

  if (!Auth::guest()) {
  return Redirect::to('admin')->with('info', 'You are already logged in.');
  }

      if (Cookie::get('auth_remember')) {
          $user_id = Crypter::decrypt(Cookie::get('auth_remember'));
          Auth::login($user_id);
          $url = Input::get('next') ? Input::get('next') : 'admin';
          return Redirect::to($url)->with('success', 'You have logged in successfully.');
      }
$this->layout->nest('content', 'admin.sessions.login');
}


    public function postLogin() {


    $credentials = array('email' => Input::get('username'), 'password' => Input::get('password'),'role'=>'admin');
         
      if (Auth::attempt($credentials)) {

          $user = User::find(Auth::user()->id);

          $user->last_login = date('Y-m-d H:i:s');
          $user->ip = getenv("REMOTE_ADDR");
          $user->save();
          $url = Input::get('next') ? Input::get('next') : 'admin';

          return Redirect::to($url)->with('success', 'You have logged in successfully.');
      }   
      else {
      return Redirect::back()->with('error', 'Invalid username or password')->withInput(Input::except('password'));
      }

    }


    public function getLogout() 
    {
      Auth::logout();
      return Redirect::to('admin/login')->with('success', 'You have logged out successfully.');
    }

    public function getChangePassword()
    { 
      $this->layout->nest('content', 'admin.sessions.changepass');
    }
    public function postChangePassword()
    {
       $user = Auth::user()->password;
        $rules = array(
        'old_password'  => array('required','alpha_num'),
        'password'  => array('required','min:6','max:20','alpha_num','confirmed','different:old_password'),
        'password_confirmation'=>array('required','alpha_num')
        );

        $validation = Validator::make(Input::all(), $rules);
    
        if ($validation->fails())
        {
            return Redirect::back()->withErrors($validation);
        }
        else
        {
            $old_password = Input::get('old_password');
            if(Hash::check($old_password,$user))
        {
            $new_pass = Hash::make(Input::get('password'));
            $user1 = User::find(Auth::user()->id);
            $user1->password = $new_pass;
            $user1->save();
            Auth::logout();
            return Redirect::to('/admin/login')->with('success','Your password successfully changed');
        }
        else
        {
            return Redirect::back()->with('error','Please enter correct old password');
        }
    }
    }


}