<?php

class Admin_InquiriesController extends Admin_BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$query = Inquiry::query();
	    if(Input::get('id')) $query->where('id','=', Input::get('id'));
	    if(Input::get('name')) $query->where('name','LIKE', Input::get('name'));
	    if(Input::get('email')) $query->where('email','LIKE', Input::get('email'));
	    $row = new Inquiry(Input::old());
	    $rows = $query->paginate();    
	    $this->layout->content = View::make('admin.inquiries.list', compact('rows','row'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rows=Inquiry::all()->find($id);
        $this->layout->content = View::make('admin.inquiries.detail', compact('rows'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$inquiry = Inquiry::find($id);
	    $inquiry->delete();
	    if(Request::ajax())
	      return Response::json('OK');
	    else
	      return Redirect::route('admin.inquiries.index');
	}


}
