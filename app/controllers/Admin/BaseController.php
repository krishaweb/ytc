<?php

class Admin_BaseController extends Controller {

  protected $layout = 'admin.layouts.base';

  /**
   * Setup the layout used by the controller.
   *
   * @return void
   */
  protected function setupLayout()
  {
    if ( ! is_null($this->layout))
    {
      $this->layout = View::make($this->layout);
    }

      $this->prefix = Request::segment(1);
      $this->controller = Request::segment(2);
      $this->action = Request::segment(3);
      View::share('prefix', $this->prefix);
      View::share('controller', $this->controller);
      View::share('action', $this->action);
      Former::framework('TwitterBootstrap3');
  }

}
