<?php

class Admin_NewsCategoriesController extends Admin_BaseController {
  
  public $restful = true;

  public function getIndex() { 
       
    $query = News_Category::query(); 
    $query->orderBy('position');
    
    $query->where(function($q) {
      if(Input::get('id')) $q->where('id','=',Input::get('id'));      
      if(Input::get('name')) $q->where('name','LIKE','%'.Input::get('name').'%');
    });

    $rows = $query->paginate();    
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.list", compact(
                            'rows'
    ));
  }

  public function getAdd() {   
    $edit = false;
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.form", compact(
                            'edit'
    ));
  }

  public function postAdd() {
    $validator = Validator::make(Input::all(), News_Category::$rules);
    if ($validator->passes()) {
      $row = new News_Category(Input::all());
      $row->save();          

      $url = Input::get('next', "{$this->prefix}/{$this->controller}");
      return Redirect::to($url)->with('success', 'Saved successfully');
    }
    Former::withErrors($validator);
    return $this->getAdd();
  }

  public function getEdit($id) {    
    $edit = true;
    $row = News_Category::find($id);   
    Former::populate( $row );
    $this->layout->content = View::make("{$this->prefix}.{$this->controller}.form", compact(
                            'edit','row'
    ));
  }
  
  public function postEdit($id) {
    News_Category::$rules['slug'] .= ',' . $id;
    $validator = Validator::make(Input::all(), News_Category::$rules);
    if ($validator->passes()) {
      $row = News_Category::find($id);
      $row->fill(Input::all());
      $row->save();

      $url = Input::get('next', "{$this->prefix}.{$this->controller}");
      return Redirect::to($url)->with('success', 'Saved successfully');
    }
    Former::withErrors($validator);
    return $this->getEdit($id);
  }
  
  public function getDelete($id) {
    $url = Input::get('next', "{$this->prefix}/{$this->controller}");
    try {      
      $row = News_Category::findOrFail($id);
      $row->delete();      
      return Redirect::to($url)->with('success', 'Deleted successfully');
    } catch(Exception $e) {
      return Redirect::to($url)->with('error', $e->getMessage());
    }
  }
  
  
}