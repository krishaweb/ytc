<?php

class BaseController extends Controller {
	protected $layout = 'layouts.base';
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}

      	$this->controller = Request::segment(1);
      	$this->action = Request::segment(2);
      	View::share('controller', $this->controller);
      	View::share('action', $this->action);
	}

}
