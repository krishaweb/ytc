<?php

class SubscribesController extends BaseController {
	public $restful = true;

	public function postIndex() {

        $rules = array(
            'email' => 'required|email|unique:subscribes',
        );
        $messages = array(
            'email.required' => 'Please enter your email address',
            'email' => 'Please enter valid email address',
            );
        $validator = Validator::make(Input::all(), $rules, $messages);
        $data = Input::all();
         if(Request::ajax())
        {
            if ($validator->passes()) {
                $subscription = new Subscribe;
                $subscription->email = $data['email'];
                $subscription->ip = Request::getClientIp();
                $subscription->ua = $_SERVER['HTTP_USER_AGENT'];

                $subscription->save();
                Mail::send('emails.subscription', array(), function($message) {
                            $message->from(Config::get('site.email'), Config::get('site.name'));
                            $message->to('nicolecross1579@gmail.com');
                            $message->bcc('sam@etraffic.com.au');
                            $message->subject('New Subscription for newsletters from ' . Config::get('site.name'));
                        });


                Mail::send('emails.subscription_ack', array(), function($message) use($subscription) {
                            $message->from(Config::get('site.email'), Config::get('site.name'));
                            $message->to($subscription->email);
                            $message->subject('Thankyou for subscribing to our newsletters.');
                        });

                return 'ok';
            }
            else{
                return $validator->messages()->get('email');
            }
        }


	}

}