<?php

class HomeController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$this->layout->content = View::make("home.index");
	}
	

	public function getAbout() {
		// $page = Content::where('code','=','about')->first();
		$this->layout->content = View::make('home.about');
	}

	public function getProvider() {
		$this->layout->content = View::make('home.provider');
	}

	public function getPrivacy() {
		// $page = Content::where('code','=','privacy')->first();
		// $blogs=Blog_Post::query()->orderBy('date','desc')->first();		
		// $this->layout->content = View::make('home.privacy',compact('page','blogs'));
	}

	public function getTerms() {
		// $page = Content::where('code','=','terms')->first();
		// $blogs=Blog_Post::query()->orderBy('date','desc')->first();		
		$this->layout->content = View::make('home.terms');
	}

	public function getThankyou() {	
	    $this->layout->content = View::make('home.thankyou');
	}

}
