<?php

class NewsController extends BaseController {

	public $restful = true;
	public $timestamps = true;
	public function getIndex($slug = null) {

		$query = News::query();

		$posts = $query->paginate(10);

		$recent_post = News::orderBy('date','desc')->take(5)->get();

		$this->layout->content = View::make('news.index',compact('posts','recent_post'));		
	}


	public function getView($id,$slug = null) {


		$post = News::find($id);
        
        if (!$post) {
            App::abort(404);
        }

        if($slug != $post->slug) {
            return Redirect::action('NewsController@getView',array($id,$post->slug));
        }

		$recent_post = News::orderBy('date','desc')->take(5)->get();

		$this->layout->content = View::make('news.view',compact('post','recent_post'));


	}
	
}