<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', 'UsersController@getLogin');
Route::post('login', 'UsersController@postLogin');
Route::get('logout', 'UsersController@getLogout')->before('auth');
Route::get('sign-up', 'UsersController@getRegister');
Route::post('sign-up', 'UsersController@postRegister');
Route::get('forgot-password', 'UsersController@getForgot');
Route::post('forgot-password', 'UsersController@postForgot');
Route::get('password/reset/{token}', 'UsersController@getReset');
Route::post('password/reset/{token}', 'UsersController@postReset');
Route::get('active/{reset_token}', array('uses'=>'UsersController@getActive','as'=>'activate'));

Route::group(array('prefix'=>'my','before'=>'auth'), function() {
  Route::get('edit-profile', 'UsersController@getProfile');
  Route::post('edit-profile', 'UsersController@postProfile');
  Route::get('change-password', 'UsersController@getChangePassword');
  Route::post('change-password', 'UsersController@postChangePassword');
  Route::get('courses', 'UsersController@getCourse');
});


Route::get('admin/login', 'Admin_SessionsController@getLogin');
Route::post('admin/login', 'Admin_SessionsController@postLogin');
Route::get('admin/change-pass','Admin_SessionsController@getChangePassword')->before('admin_auth');
Route::post('admin/change-pass','Admin_SessionsController@postChangePassword')->before('admin_auth');
Route::get('admin/logout', 'Admin_SessionsController@getLogout')->before('admin_auth');





Route::group(array('prefix'=>'admin','before'=>'admin_auth'), function() {

  Route::get('/', 'Admin_DashboardController@getIndex');
  Route::controller('news_categories', 'Admin_NewsCategoriesController');
  Route::resource('news','Admin_NewsController');
  Route::controller('course_categories', 'Admin_CourseCategoriesController');
  Route::resource('courses','Admin_CoursesController');
  Route::resource('inquiries','Admin_InquiriesController',array('except'=>array('show,edit,update')));
  Route::controller('settings', 'Admin_SettingsController');
  Route::resource('contents','Admin_ContentsController',array('except'=>array('show')));
  Route::resource('resources', 'Admin_ResourcesController');
  Route::resource('subscribes','Admin_SubscribesController');
});


Route::get('/', 'HomeController@getIndex');
Route::get('news', array('uses'=>'NewsController@getIndex'));
Route::get('news/{id}-{slug}', array('uses'=>'NewsController@getView'))
   ->where(array('id'=>'[0-9]+'));
Route::get('resources', array('uses'=>'ResourcesController@getIndex'));
Route::get('download/{file}', array('uses'=>'ResourcesController@getDownload'));
Route::controller('contact-us','InquiriesController');
Route::get('about-us', array('uses'=>'HomeController@getAbout'));
Route::get('for-providers', array('uses'=>'HomeController@getProvider'));
Route::get('choose-training', array('uses'=>'CoursesController@getChoose'));
Route::post('subscribes', array('uses'=>'SubscribesController@postIndex','as'=>'subscribes'));
Route::get('terms-and-conditions', array('uses'=>'HomeController@getTerms'));
Route::get('thankyou', array('uses'=>'HomeController@getThankyou','as'=>'thankyou'));
Route::get('courses', array('uses'=>'CoursesController@getIndex','as'=>'listing'));


// ===============================================
	// 404 ===========================================
	// ===============================================

	App::missing(function($exception)
	{

		// shows an error page (app/views/error.blade.php)
		// returns a page not found error
		return Response::view("error.notfound");
	});
