<?php
class Course extends Eloquent{
    use Iverberk\Larasearch\Traits\SearchableTrait;

	protected $table = 'courses';
    protected $fillable = array('category_id','provider_id','title','slug','photo','description','country','location','profession','level','accreditation','sector','delivery_method','subject','course_date','price','created_at','updated_at','status');
    public static $rules = array(
        'category_id' => 'required',
        'provider_id' => 'required',
        'title' => 'required',
        'description' => 'required',
        'photo'=>'required',
        'country' => 'required',
        'location' => 'required',
        'profession' => 'required',
        'level' => 'required',
        'accreditation' => 'required',
        'sector' => 'required',
        'delivery_method' => 'required',
        'subject' => 'required',
        'course_date' => 'required',
        'price' => 'required'
    );

    public static $sluggable = array(
        'build_from' => 'title',
        'save_to' => 'slug',
        'on_update' => true,
    );
   
    public function getDates() {
        return array('created_at', 'updated_at','course_date');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function category() {
        return $this->belongsTo('Course_Category', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function provider() {
        return $this->belongsTo('User', 'provider_id');
    }

    public function setPhotoAttribute($file) {
        $source_path = upload_tmp_path($file);

        if ($file && file_exists($source_path)) {

            upload_move($file, 'courses');


            Image::make($source_path)->resize(694, 318)->save($source_path);
            upload_move($file, 'courses', 'medium');

            Image::make($source_path)->resize(529, 363)->save($source_path);
            upload_move($file, 'courses', 'thumb');

            @unlink($source_path);

            $this->deleteFile();
        }
        $this->attributes['photo'] = $file;
        if ($file == '') {
            $this->deleteFile();
            $this->attributes['photo'] = "";
        }
    }

    public function photo_url($type = 'original') {

        if (!empty($this->photo))
            return upload_url($this->photo, 'courses', $type);
        elseif (!empty($this->photo) && file_exists(tmp_path($this->photo)))
            return tmp_url($this->$photo);
        else
            return "http://placehold.it/64x64";
    }

    public function deleteFile() {
        upload_delete($this->photo, 'courses', array('original', 'thumb', 'medium'));
    }

}
Event::listen('eloquent.deleting: Course', function($model) {
  $model->deleteFile();
});
