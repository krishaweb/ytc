<?php

class Subscribe extends Eloquent {
	
	protected $table = "subscribes";

	protected $fillable = array('email','ip','ua');	
    
    public $timestamps = true;

     public function getDates() {
        return array('created_at');
    }

}
