<?php

class Course_Category extends Baum\Node {
  
  use Iverberk\Larasearch\Traits\SearchableTrait;
  
  protected $table = 'course_categories';
  protected $fillable = array('name', 'slug', 'parent_id');
  public $timestamps = true;

   public static $sluggable = array(
      'build_from' => 'name',
      'save_to' => 'slug',
      'on_update' => true
  );  

   public static $rules = array(
      'name' => 'required'
  );

  /**
   * @return \Illuminate\Database\Eloquent\Relations
   */
   public function courses()
    {
        return $this->hasMany('Course', 'category_id');
    }

   public static function categories($blank = "") {

    $list = self::lists('name','id');

    if ($blank === FALSE)
      return $list;

    return array("" => $blank) + $list;
    
  }

}
