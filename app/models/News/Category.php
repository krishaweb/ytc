<?php

class News_Category extends Eloquent {
  
  protected $table = 'news_categories';
  
  public $timestamps = false;

  protected $fillable = array('name','slug','position');

  public static $sortable = true;

  public static $sluggable = array(
      'build_from' => 'name',
      'save_to' => 'slug',
      'on_update' => true
  );  
  
  public static $rules = array(
      'name' => 'required|max:255',
      'slug' => 'unique:news_categories,slug'
  );
  
  public function posts() {
      return $this->hasMany('News','category_id')->where('status','=',1);
  }

  public static function categories($blank = "") {

  	$list = self::lists('name','id');

    if ($blank === FALSE)
      return $list;

    return array("" => $blank) + $list;
    
  }
  
}

Event::listen('eloquent.saving: News_Category', function($model, $force = false, $objects = null) {

   $class = get_class($model);   
  // If the class has no sluggable configuration, there is nothing to be done so we stop.
  if ($model->position || !isset($class::$sortable))
  {
    
      return;
  }    
  $max = $class::max('position'); 
  $model->position = $max+1;  
});