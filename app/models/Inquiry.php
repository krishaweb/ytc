<?php

class Inquiry extends Eloquent {
	
	protected $table = "inquiries";

	protected $fillable = array('name','phone', 'email','message','ip','ua');	
    
    public $timestamps = false;

     public function getDates() {
        return array('created_at');
    }

}
