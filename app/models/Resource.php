<?php

class Resource extends Eloquent {

    protected $table = 'resources';
    protected $fillable = array('title','type','pdf','link','status','created_at', 'updated_at');
    public static $rules = array(
        'title' => 'required',
        
    );
  
    public static $types = array(
        'pdf' => 'PDF',
        'link' => 'Link'
    );

    public function setPdfAttribute($file) {
        $source_path = upload_tmp_path($file);
        if ($file && file_exists($source_path)) {
            upload_move($file, 'resources');
            @unlink($source_path);
            $this->deleteFile();
        }
        $this->attributes['pdf'] = $file;
        if ($file == '') {
            $this->deleteFile();
            $this->attributes['pdf'] = "";
        }
    }

    public function pdf_url($type = 'original') {

        if (!empty($this->pdf))
            return upload_url($this->pdf, 'resources', $type);
        elseif (!empty($this->pdf) && file_exists(tmp_path($this->pdf)))
            return tmp_url($this->pdf);
        else
            return "http://placehold.it/64x64";
    }

    public function deleteFile() {
        upload_delete($this->pdf, 'resources', array('original'));
    }

    
}
Event::listen('eloquent.deleting: Resource', function($model) {
  $model->deleteFile();
});


