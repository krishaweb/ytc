<?php

class News extends Eloquent {

    protected $table = 'news';
    protected $fillable = array('status','title','slug','date', 'content','intro', 'created_at', 'updated_at','meta_title','meta_keywords','meta_description','photo');
    public static $rules = array(
        'title' => 'required',
        'intro' => 'required',
        'content' => 'required',
        'photo'=>'required'
    );

    public static $sluggable = array(
        'build_from' => 'title',
        'save_to' => 'slug',
        'on_update' => true,
    );
   
    public function getDates() {
        return array('created_at', 'updated_at','date');
    }

    public function category() {
        return $this->belongsTo('News_Category', 'category_id');
    }

    public function setPhotoAttribute($file) {
        $source_path = upload_tmp_path($file);

        if ($file && file_exists($source_path)) {

            upload_move($file, 'news');


            Image::make($source_path)->resize(619, 290)->save($source_path);
            upload_move($file, 'news', 'medium');

            Image::make($source_path)->resize(71, 70)->save($source_path);
            upload_move($file, 'news', 'thumb');

            @unlink($source_path);

            $this->deleteFile();
        }
        $this->attributes['photo'] = $file;
        if ($file == '') {
            $this->deleteFile();
            $this->attributes['photo'] = "";
        }
    }

    public function photo_url($type = 'original') {

        if (!empty($this->photo))
            return upload_url($this->photo, 'news', $type);
        elseif (!empty($this->photo) && file_exists(tmp_path($this->photo)))
            return tmp_url($this->$photo);
        else
            return "http://placehold.it/64x64";
    }

    public function deleteFile() {
        upload_delete($this->photo, 'news', array('original', 'thumb', 'medium'));
    }

    
}
Event::listen('eloquent.deleting: News', function($model) {
  $model->deleteFile();
});


