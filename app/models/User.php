<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use Iverberk\Larasearch\Traits\SearchableTrait;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	
	public static $rules=array
		(
			'fname'=>'required',
			'lname'=>'required',
			'email'=>'required|email|unique:users',
			'password'=>'required|alpha_num|min:6|max:20|confirmed',
			'password_confirmation'=>'required'
		);

	protected $fillable=array
		(
			'name',
			'email',
			'phone',
			'url',
			'username',
			'password',
			'subscribed',
			'status',
			'admin',
			'reset_token',
			'ip',
			'last_login',
		);

	public function scopeProvider($query)
    {
        return $query->where('role', '=', 'provider');
    }

  /**
   * @return \Illuminate\Database\Eloquent\Relations
   */
    public function courses()
    {
        return $this->hasMany('Course', 'provider_id');
    }

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function __toString() {
		return $this->name;
	}

}
