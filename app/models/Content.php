<?php

/**
* 
*/
class Content extends Eloquent
{
	public static $rules=array(
		'code'=>'required|unique:contents,code',
		'title'=>'required',
		'description'=>'required',
		
		);
	protected $fillable=array(
		'code',
		'title',
		'description',
		'meta_title',
		'meta_keywords',
		'meta_description'
		);
}
