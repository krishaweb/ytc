<?php
$settings = Cache::remember('settings', 60, function() {
                  return DB::table('settings')->lists('value', 'id');
                });
    foreach ($settings as $id => $value)
      Config::set($id, $value);

function seat_dropdown()
{
  $ret = array(
      '' => 'Select Options',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
      6 => '6',
      7 => '7',
      8 => '8'
  );
  return $ret;
}
function assoc_to_radios($assoc, $name) {
  $ret = array();
  foreach ($assoc as $k => $v) {
    $ret[$v] = array(
        'name' => $name,
        'value' => $k,
        'id' => str_replace(array("[","]"),"",$name).'_'.$k
    );
  }
  return $ret;
}

function month_dropdown() {
  $ret = array(
      '' => '-- Month --',
      1 => 'Jan',
      2 => 'Feb',
      3 => 'Mar',
      4 => 'Apr',
      5 => 'May',
      6 => 'Jun',
      7 => 'Jul',
      8 => 'Aug',
      9 => 'Sep',
      10 => 'Oct',
      11 => 'Nov',
      12 => 'Dec'
  );
  return $ret;
}

function year_dropdown() {
  $ret = array('' => '-- YEAR --');
  for($i=date('Y');$i>1900;$i--) {
    $ret[$i] = $i;
  } 
  return $ret;
}

function get_month(){
  $expiry_month = array();

    for($i = 1; $i <= 12; $i++) {
        $expiry_month[sprintf('%02d', $i)] = sprintf('%02d', $i);
    }
    return $expiry_month;
}

function get_year(){
  $ret = array();
  $i = date("y");
  $j = $i+11;
  for ($i; $i <= $j; $i++) {
      $ret[sprintf('%02d', $i)] = sprintf('%02d', $i);
  }
  return $ret;
}




function upload_tmp_path($file) {

  return public_path() . "/a/tmp/$file";
}

function upload_tmp_url($file) {
  return asset("/a/tmp/$file");
}

function upload_path($file, $model, $variation=false, $relative=null) {
  $use_aws = is_null($relative)? Config::get('aws.use',false) : $relative;
  $folder = "/a/uploads/". ( empty($variation) || $variation =='original' ? $model : "{$model}-{$variation}" );
  if (!$use_aws && !is_array($variation) && !file_exists(public_path().$folder)) {
      umask(0);
      @mkdir(public_path().$folder, 0777, true);
    }
  $target_path = ($use_aws? "" : public_path()). "$folder/$file";
  return $target_path;
}

function upload_url($file, $model, $variation=false)  {
  $use_aws = Config::get('aws.use',false);
  $folder = "/a/uploads/". ( empty($variation)  || $variation =='original' ? $model : "{$model}-{$variation}" );
  if(!empty($file)) {
    if($use_aws)
      return Config::get('aws.host')."$folder/$file";
    else
      return asset("$folder/$file");
  } 
  return false;
}

function upload_move($file,$model,$variation=false) {
  $use_aws = Config::get('aws.use',false);
  $source = upload_tmp_path($file);
  $target = upload_path($file,$model,$variation);

  if($use_aws) {    
    AWS::get('s3')->putObject(array(
            'Bucket'     => Config::get('aws.bucket'),
            'Key'        => $target,
            'SourceFile' => $source,
            'ACL'    => 'public-read'
        ));
  } else {
    copy($source, $target);
  }
}

function upload_delete($file,$model,$variations=false) {
  if(empty($file)) return false;;
  if(is_array($variations)) {
    foreach($variations as $variation){
        $local_path = upload_path($file,$model,$variation,false);
        @unlink($local_path);
    } 
  }


  $use_aws = Config::get('aws.use',false);    
  if($use_aws) {
    $aws_path = upload_path($file,$model,$variations,true);
    AWS::get('s3')->deleteObject(array(
          'Bucket'     => Config::get('aws.bucket'),
          'Key'        => $aws_path,          
        ));     
  }

}

function exec_artisan($task,$ret=false) {
    $is = App::environment() != 'local';
    $php = $is? "/opt/php53/bin/php" : 'php';
    $cmd = "$php ".base_path()."/artisan $task --env=".App::environment();
    shell_exec($cmd);
    if($ret) die($cmd);
}
